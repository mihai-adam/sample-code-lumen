<?php

$router->group(['prefix' => 'account', 'as' => 'account'], function ($router) {
    $router->get('', ['as' => 'index', 'uses' => 'AccountController@index']);
    $router->get('/count', ['as' => 'count', 'uses' => 'AccountController@count']);
    $router->get('/{account}', ['as' => 'show', 'uses' => 'AccountController@show']);
    $router->post('', ['as' => 'create', 'uses' => 'AccountController@create']);
    $router->patch('', ['as' => 'update', 'uses' => 'AccountController@update']);
    $router->delete('', ['as' => 'delete', 'uses' => 'AccountController@delete']);
});


$router->group(['prefix' => 'user', 'as' => 'user'], function ($router) {

    $router->group(['prefix' => '/{user}/group', 'as' => 'group'], function ($router) {
        $router->get('', ['as' => 'index', 'uses' => 'UserGroupController@index']);
        $router->get('/count', ['as' => 'count', 'uses' => 'UserGroupController@count']);
        $router->post('{group}', ['as' => 'attach', 'uses' => 'UserGroupController@attach']);
        $router->put('', ['as' => 'sync', 'uses' => 'UserGroupController@sync']);
        $router->delete('{group}', ['as' => 'detach', 'uses' => 'UserGroupController@detach']);
    });

    $router->group(['prefix' => '/{user}/action', 'as' => 'action'], function ($router) {
        $router->get('', ['as' => 'index', 'uses' => 'UserActionController@index']);
        $router->get('/count', ['as' => 'count', 'uses' => 'UserActionController@count']);
        $router->post('{action}', ['as' => 'attach', 'uses' => 'UserActionController@attach']);
        $router->put('', ['as' => 'sync', 'uses' => 'UserActionController@sync']);
        $router->delete('{action}', ['as' => 'detach', 'uses' => 'UserActionController@detach']);
    });

    $router->group(['prefix' => '/batch', 'as' => 'batch'], function ($router) {
        $router->post('', ['as' => 'store', 'uses' => 'UserBatchController@store']);
        $router->patch('', ['as' => 'update', 'uses' => 'UserBatchController@update']);
        $router->delete('', ['as' => 'delete', 'uses' => 'UserBatchController@delete']);
    });

    $router->get('', ['as' => 'index', 'uses' => 'UserController@index']);
    $router->get('/count', ['as' => 'count', 'uses' => 'UserController@count']);
    $router->get('/{user}', ['as' => 'show', 'uses' => 'UserController@show']);
    $router->post('', ['as' => 'create', 'uses' => 'UserController@create']);
    $router->patch('/{user}', ['as' => 'update', 'uses' => 'UserController@update']);
    $router->delete('/{user}', ['as' => 'delete', 'uses' => 'UserController@delete']);
    $router->post('/login', ['as' => 'login', 'uses' => 'UserController@login']);

});

$router->group(['prefix' => 'group', 'as' => 'group'], function ($router) {

    $router->group(['prefix' => '/{group}/user', 'as' => 'user'], function ($router) {
        $router->get('', ['as' => 'index', 'uses' => 'GroupUserController@index']);
        $router->get('/count', ['as' => 'count', 'uses' => 'GroupUserController@count']);
        $router->post('{user}', ['as' => 'attach', 'uses' => 'GroupUserController@attach']);
        $router->put('', ['as' => 'sync', 'uses' => 'GroupUserController@sync']);
        $router->delete('{user}', ['as' => 'detach', 'uses' => 'GroupUserController@detach']);
    });

    $router->group(['prefix' => '/{group}/action', 'as' => 'action'], function ($router) {
        $router->get('', ['as' => 'index', 'uses' => 'GroupActionController@index']);
        $router->get('/count', ['as' => 'count', 'uses' => 'GroupActionController@count']);
        $router->post('{action}', ['as' => 'attach', 'uses' => 'GroupActionController@attach']);
        $router->put('', ['as' => 'sync', 'uses' => 'GroupActionController@sync']);
        $router->delete('{action}', ['as' => 'detach', 'uses' => 'GroupActionController@detach']);
    });

    $router->group(['prefix' => '/batch', 'as' => 'batch'], function ($router) {
        $router->post('', ['as' => 'store', 'uses' => 'GroupBatchController@store']);
        $router->patch('', ['as' => 'update', 'uses' => 'GroupBatchController@update']);
        $router->delete('', ['as' => 'delete', 'uses' => 'GroupBatchController@delete']);
    });

    $router->get('', ['as' => 'index', 'uses' => 'GroupController@index']);
    $router->get('/count', ['as' => 'count', 'uses' => 'GroupController@count']);
    $router->post('', ['as' => 'store', 'uses' => 'GroupController@store']);
    $router->get('/{group}', ['as' => 'show', 'uses' => 'GroupController@show']);
    $router->patch('/{group}', ['as' => 'update', 'uses' => 'GroupController@update']);
    $router->delete('', ['as' => 'delete', 'uses' => 'GroupController@delete']);

});

$router->group(['prefix' => 'action', 'as' => 'action'], function ($router) {

    $router->group(['prefix' => '/batch', 'as' => 'batch'], function ($router) {
        $router->post('', ['as' => 'store', 'uses' => 'ActionBatchController@store']);
        $router->patch('', ['as' => 'update', 'uses' => 'ActionBatchController@update']);
        $router->delete('', ['as' => 'delete', 'uses' => 'ActionBatchController@delete']);
    });

    $router->get('', ['as' => 'index', 'uses' => 'ActionController@index']);
    $router->get('{action}', ['as' => 'show', 'uses' => 'ActionController@show']);
    $router->post('', ['as' => 'store', 'uses' => 'ActionController@store']);
    $router->patch('{action}', ['as' => 'update', 'uses' => 'ActionController@update']);
    $router->delete('{action}', ['as' => 'delete', 'uses' => 'ActionController@delete']);
});


/**
 * Api docs
 */
$router->get('api-docs', ['as' => 'api-docs.index', 'uses' => 'ApiDocsController@index']);