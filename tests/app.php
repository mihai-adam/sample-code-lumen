<?php
/**
 * Created by PhpStorm.
 * User: mihai
 * Date: 21.09.2018
 * Time: 02:13
 */
return;
$app = require dirname(__FILE__) . '/../bootstrap/app.php';

function phpunit_boot()
{
    $host = getenv("DB_HOST");
    $database = getenv('DB_DATABASE');
    $username = getenv('DB_USERNAME');
    $password = getenv('DB_PASSWORD');

    $conn = mysqli_connect($host, $username, $password);
// Check connection
    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }
    //remove if exists
    $sql = "DROP DATABASE IF EXISTS `" . $database . "`;";
    if (mysqli_query($conn, $sql)) {
        echo "Prev. database is clean now" . PHP_EOL;
    } else {
        echo "Unable to clean existing db: " . mysqli_error($conn);
    }

// Create database
    $sql = "CREATE DATABASE `" . $database . "`;";
    if (mysqli_query($conn, $sql)) {
        echo "Database created successfully" . PHP_EOL;
    } else {
        echo "Error creating database: " . mysqli_error($conn);
    }

    mysqli_close($conn);
}

phpunit_boot();


try {
    echo "Migrating" . PHP_EOL;
    $app['Illuminate\Contracts\Console\Kernel']->call('migrate');
} catch (\Exception $e) {
    echo "Fail to migrate" . PHP_EOL;
    throw $e;
}

echo "Completed migration" . PHP_EOL;
echo "Start tests" . PHP_EOL;


