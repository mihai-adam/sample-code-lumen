<?php

namespace Tests;

use Faker\Factory;
use Laravel\Lumen\Testing\TestCase as LumenTestCase;
use phpDocumentor\Reflection\DocBlock\Tags\Property;
use ReflectionClass;
use ReflectionProperty;

abstract class TestCase extends LumenTestCase
{

    protected $faker;


    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        $app = require __DIR__ . '/../bootstrap/app.php';
        return $app;
    }


    protected function faker()
    {
        if (null == $this->faker) {
            $this->faker = Factory::create();
        }
        return $this->faker;
    }
}
