<?php

namespace Tests\Unit\App\Http\Filters;

use App\Http\Filters\Exceptions\HttpFilterException;
use App\Http\Filters\Filter;
use App\Http\Filters\Manager;
use App\Http\Filters\Standard\FieldsetFilter;
use App\Http\Filters\Standard\LimitFilter;
use App\Http\Filters\Standard\OffsetFilter;
use App\Http\Filters\Standard\SortFilter;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use ReflectionClass;
use Tests\TestCase;

/**
 * Class ManagerTest
 * @package Tests\Unit\App\Http\Filter
 */
class ManagerTest extends TestCase
{

    /**
     * @see Manager::__construct()
     */
    public function testConstructMethod()
    {
        $request = new Request([
            'filter1' => 'value1',
            'filter2' => 'value2'
        ]);

        $manager = new Manager($request);

        $this->assertAttributeEquals(
            $request,
            'request',
            $manager,
            'Right request value'
        );
    }

    /**
     * @see Manager::request()
     */
    public function testRequestMethod()
    {
        $request = new Request([
            'filter1' => 'value1',
            'filter2' => 'value2'
        ]);

        $manager = new Manager($request);

        $this->assertEquals(
            $manager->request(),
            $request,
            'Right method output'
        );

    }

    /**
     * @see Manager::count()
     */
    public function testCountMethod()
    {
        $request = new Request([
            'filter1' => 'value1',
            'filter2' => 'value2'
        ]);

        $manager = new Manager($request);
        $manager->count(true);

        $this->assertAttributeEquals(
            ['sort', 'limit', 'offset'],
            'disabled',
            $manager,
            'Right disabled value'
        );

        $manager->count(false);

        $this->assertAttributeEmpty(
            'disabled',
            $manager,
            'Right disabled value'
        );
    }

    /**
     * @see Manager::enable()
     */
    public function testEnableMethod()
    {
        $request = new Request([
            'filter1' => 'value1',
            'filter2' => 'value2'
        ]);

        $manager = new Manager($request);
        $manager->disable('a_field');
        $output = $manager->enable('a_field');

        $this->assertTrue(Manager::class == get_class($output));


        $this->assertAttributeEmpty(
            'disabled',
            $manager,
            'Right disabled value'
        );
    }

    /**
     * @see Manager::disable()
     */
    public function testDisableMethod()
    {
        $request = new Request([
            'filter1' => 'value1',
            'filter2' => 'value2'
        ]);

        $manager = new Manager($request);

        $output = $manager->disable('a_field');

        $this->assertInstanceOf(
            Manager::class,
            $output,
            'Right output type');

        $this->assertAttributeEquals(
            ['a_field'],
            'disabled',
            $manager,
            'Right disabled value'
        );
    }

    /**
     * @see Manager::disabled()
     */
    public function testDisabledMethod()
    {
        $request = new Request([
            'filter1' => 'value1',
            'filter2' => 'value2'
        ]);

        $manager = new Manager($request);

        $manager->disable('a_field');

        $this->assertAttributeEquals(
            $manager->disabled(),
            'disabled',
            $manager,
            'Right disabled value'
        );
    }

    /**
     * @see Manager::enabled()
     */
    public function testEnabledMethod()
    {
        $query = [
            'filter1' => 'value1',
            'filter2' => 'value2'
        ];
        $request = new Request($query);
        $manager = new Manager($request);
        $manager->disable('filter2');

        $this->assertEquals(
            ['filter1'],
            $manager->enabled($query),
            'Right enabled fields'
        );
        $manager->disable('filter1');
        $this->assertEmpty(
            $manager->enabled($query),
            'Right enabled fields'
        );

    }

    /**
     * Test when all filters are available and return default Filter object
     * @see Manager::collect()
     */
    public function testCollectMethodWithStandardFilters()
    {
        $query = [
            'filter1' => 'value1',
            'filter2' => 'value2'
        ];
        $request = new Request($query);
        $manager = new Manager($request);

        $output = $manager->collect($query);
        $this->assertInstanceOf(
            Collection::class,
            $output,
            ''
        );
        $this->assertEquals(
            2,
            $output->count(),
            'Right collection length'
        );
    }

    /**
     * Test when a filter don't has a valid value and throws exception
     * @see Manager::collect()
     */
    public function testCollectMethodWithFilterException()
    {
        $query = [
            'filter1' => 'value1:wrong:value',
            'filter2' => 'value2'
        ];
        $request = new Request($query);
        $manager = new Manager($request);

        $this->expectException(HttpFilterException::class);

        $manager->collect($query);
    }

    /**
     * Test collect when a custom method exists to generate a filter
     * @see Manager::collect()
     */
    public function testCollectMethodWithCustomFilter()
    {
        $class = Manager::class;
        $query = [
            'custom' => 'value1',
            'another_filter' => 'value2'
        ];
        $request = new Request($query);

        $mock = $this->getMockBuilder($class)
            ->disableOriginalConstructor()
            ->setMethods([
                'createCustomFilter',
                'createAnotherFilterFilter'
            ])
            ->getMock();

        $mock->expects($this->once())
            ->method('createCustomFilter')
            ->will($this->returnValue(new Filter('name', 'value')));
        $mock->expects($this->once())
            ->method('createAnotherFilterFilter')
            ->will($this->returnValue(new Filter('name', 'value')));

        // now call the constructor
        $reflectedClass = new ReflectionClass($class);
        $constructor = $reflectedClass->getConstructor();
        $constructor->invoke($mock, $request);

        $mock->collect(array_keys($query));
    }

    /**
     * @see Manager::createSortFilter()
     * @throws \ReflectionException
     */
    public function testCreateSortFilter()
    {
        $request = new Request();
        $manager = new Manager($request);

        $class = new ReflectionClass(Manager::class);
        $method = $class->getMethod('createSortFilter');
        $method->setAccessible(true);

        $filter = $method->invokeArgs($manager, [
            ['value' => '-field']
        ]);

        $this->assertInstanceOf(SortFilter::class, $filter);

        $filter = $method->invokeArgs($manager, [
            ['value' => null]
        ]);

        $this->assertNull($filter);


    }

    /**
     * @see Manager::createLimitFilter()
     */
    public function testCreateLimitFilter()
    {
        $request = new Request();
        $manager = new Manager($request);

        $class = new ReflectionClass(Manager::class);
        $method = $class->getMethod('createLimitFilter');
        $method->setAccessible(true);

        $filter = $method->invokeArgs($manager, [
            ['value' => 1]
        ]);

        $this->assertInstanceOf(LimitFilter::class, $filter);

        $filter = $method->invokeArgs($manager, [
            ['value' => null]
        ]);

        $this->assertNull($filter);

    }

    /**
     * @see Manager::createOffsetFilter()
     */
    public function testCreateOffsetFilter()
    {
        $request = new Request();
        $manager = new Manager($request);

        $class = new ReflectionClass(Manager::class);
        $method = $class->getMethod('createOffsetFilter');
        $method->setAccessible(true);

        $filter = $method->invokeArgs($manager, [
            ['value' => 1]
        ]);

        $this->assertInstanceOf(OffsetFilter::class, $filter);

        $filter = $method->invokeArgs($manager, [
            ['value' => null]
        ]);

        $this->assertNull($filter);

    }

    /**
     * @see Manager::createFieldsetFilter()
     */
    public function testCreateFieldsetFilter()
    {
        $request = new Request();
        $manager = new Manager($request);

        $class = new ReflectionClass(Manager::class);
        $method = $class->getMethod('createFieldsetFilter');
        $method->setAccessible(true);

        $filter = $method->invokeArgs($manager, [
            ['value' => 'fields']
        ]);

        $this->assertInstanceOf(FieldsetFilter::class, $filter);

        $filter = $method->invokeArgs($manager, [
            ['value' => null]
        ]);

        $this->assertNull($filter);

    }
}