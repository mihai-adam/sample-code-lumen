<?php

namespace Tests\Unit\App\Http\Filters\Standard;

use App\Http\Filters\Standard\OffsetFilter;
use Tests\TestCase;

/**
 * Class OffsetFilterTest
 * @package Tests\Unit\App\Http\Filters\Standard
 */
class OffsetFilterTest extends TestCase
{

    /**
     * @see OffsetFilter::getOperator()
     */
    public function testGetOperatorMethod()
    {
        $filter = new OffsetFilter(2);

        $this->assertEquals(
            'offset',
            $filter->getOperator(),
            'Right method output'
        );

    }
}