<?php

namespace Tests\Unit\App\Http\Filters\Standard;

use App\Http\Filters\Standard\LimitFilter;
use Tests\TestCase;

/**
 * Class LimitFilterTest
 * @package Tests\Unit\App\Http\Filters\Standard
 */
class LimitFilterTest extends TestCase
{

    /**
     * @see FieldsetFilter::__construct()
     */
    public function testConstructMethod()
    {
        $filter = new LimitFilter(25);

        $this->assertAttributeEquals(
            25,
            'value',
            $filter,
            'Right value'
        );

        $filter = new LimitFilter('25');

        $this->assertAttributeEquals(
            25,
            'value',
            $filter,
            'Right value'
        );

        $filter = new LimitFilter('string');

        $this->assertAttributeEquals(
            0,
            'value',
            $filter,
            'Right value'
        );
    }

    /**
     * @see FieldsetFilter::getValue()
     */
    public function testGetValueMethod()
    {
        $value = '7';
        $filter = new LimitFilter($value);

        $this->assertEquals(
            7,
            $filter->getValue(),
            'Right method output'
        );
    }

    /**
     * @see FieldsetFilter::getTarget()
     */
    public function testGetTargetMethod()
    {
        $value = $this->faker()->text(12);
        $filter = new LimitFilter($value);

        $this->assertNull(
            $filter->getTarget(),
            'Right method output'
        );
    }

    /**
     * @see FieldsetFilter::getOperator()
     */
    public function testGetOperatorMethod()
    {
        $value = $this->faker()->text(12);
        $filter = new LimitFilter($value);

        $this->assertEquals(
            'limit',
            $filter->getOperator(),
            'Right method output'
        );
    }


}