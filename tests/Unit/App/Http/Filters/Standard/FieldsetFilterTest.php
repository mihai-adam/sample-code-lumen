<?php

namespace Tests\Unit\App\Http\Filters\Standard;

use App\Http\Filters\Standard\FieldsetFilter;
use Tests\TestCase;

/**
 * Class FieldsetFilterTest
 * @package Tests\Unit\App\Http\Filters\Standard
 */
class FieldsetFilterTest extends TestCase
{

    /**
     * @see FieldsetFilter::__construct()
     */
    public function testConstructMethod()
    {
        $filter = new FieldsetFilter('a,b,c');

        $this->assertAttributeEquals(
            ['a', 'b', 'c'],
            'value',
            $filter,
            'Right value'
        );

        $filter = new FieldsetFilter('abc');
        $this->assertAttributeEquals(
            ['abc'],
            'value',
            $filter,
            'Right value'
        );
    }

    /**
     * @see FieldsetFilter::getValue()
     */
    public function testGetValueMethod()
    {
        $value = $this->faker()->text(12);
        $filter = new FieldsetFilter($value);

        $this->assertEquals(
            [$value],
            $filter->getValue(),
            'Right method output'
        );
    }

    /**
     * @see FieldsetFilter::getTarget()
     */
    public function testGetTargetMethod()
    {
        $value = $this->faker()->text(12);
        $filter = new FieldsetFilter($value);

        $this->assertNull(
            $filter->getTarget(),
            'Right method output'
        );
    }

    /**
     * @see FieldsetFilter::getOperator()
     */
    public function testGetOperatorMethod()
    {
        $value = $this->faker()->text(12);
        $filter = new FieldsetFilter($value);

        $this->assertEquals(
            'with',
            $filter->getOperator(),
            'Right method output'
        );
    }

    /**
     * @see FieldsetFilter::hasField()
     */
    public function testHasFieldMethod()
    {
        $value = 'a,b,c';
        $filter = new FieldsetFilter($value);

        $this->assertTrue(
            $filter->hasField('a'),
            'Right method output'
        );
        $this->assertFalse(
            $filter->hasField('d'),
            'Right method output'
        );
    }
}