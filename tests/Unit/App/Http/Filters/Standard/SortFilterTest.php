<?php

namespace Tests\Unit\App\Http\Filters\Standard;

use Tests\TestCase;
use App\Http\Filters\Standard\SortFilter;

/**
 * Class SortFilterTest
 * @package Tests\Unit\App\Http\Filters\Standard
 */
class SortFilterTest extends TestCase
{
    /**
     * @see FieldsetFilter::__construct()
     */
    public function testConstructMethod()
    {
        $filter = new SortFilter('-field');

        $this->assertAttributeEquals(
            '-',
            'value',
            $filter,
            'Right value'
        );
        $this->assertAttributeEquals(
            'field',
            'target',
            $filter,
            'Right value'
        );

        $filter = new SortFilter('field');

        $this->assertAttributeEquals(
            '+',
            'value',
            $filter,
            'Right value'
        );
        $this->assertAttributeEquals(
            'field',
            'target',
            $filter,
            'Right value'
        );

    }

    /**
     * @see FieldsetFilter::getValue()
     */
    public function testGetValueMethod()
    {
        $value = '-field';
        $filter = new SortFilter($value);

        $this->assertEquals(
            '-',
            $filter->getValue(),
            'Right method output'
        );
        $value = 'field';
        $filter = new SortFilter($value);

        $this->assertEquals(
            '+',
            $filter->getValue(),
            'Right method output'
        );
    }

    /**
     * @see FieldsetFilter::getTarget()
     */
    public function testGetTargetMethod()
    {
        $value = '-field';
        $filter = new SortFilter($value);

        $this->assertEquals(
            'field',
            $filter->getTarget(),
            'Right method output'
        );

        $value = 'field';
        $filter = new SortFilter($value);

        $this->assertEquals(
            'field',
            $filter->getTarget(),
            'Right method output'
        );
    }

    /**
     * @see FieldsetFilter::getOperator()
     */
    public function testGetOperatorMethod()
    {
        $value = $this->faker()->text(12);
        $filter = new SortFilter($value);

        $this->assertEquals(
            'sort',
            $filter->getOperator(),
            'Right method output'
        );
    }

}