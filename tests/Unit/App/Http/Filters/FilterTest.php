<?php

namespace Tests\Unit\App\Http\Filters;

use App\Http\Filters\Exceptions\HttpFilterException;
use ReflectionClass;
use Tests\TestCase;
use App\Http\Filters\Filter;

/**
 * Class FilterTest
 * @package Tests\Unit\App\Http\Filter
 */
class FilterTest extends TestCase
{
    /**
     * @see Filter::__construct()
     */
    public function testConstructMethod()
    {
        $filter = new Filter('field', 'value');

        $this->assertAttributeEquals(
            'field',
            'target',
            $filter,
            'Right attribute target'
        );
        $this->assertAttributeEquals(
            'value',
            'input',
            $filter,
            'Right attribute input'
        );
        $this->assertAttributeEquals(
            'value',
            'value',
            $filter,
            'Right attribute value'
        );
    }

    /**
     * Test setInput will call setNumericInput
     * @see Filter::setInput()
     */
    public function testSetInputMethodWithNumericValue()
    {
        $class = Filter::class;
        $mock = $this->getMockBuilder($class)
            ->disableOriginalConstructor()
            ->setMethods(['setNumericInput'])
            ->getMock();
        $mock->expects($this->once())
            ->method('setNumericInput')
            ->will($this->returnValue(true));

        $class = new ReflectionClass($class);
        $method = $class->getMethod('setInput');
        $method->setAccessible(true);
        $method->invoke($mock, 25);

    }

    /**
     * Test setInput will call setBoolInput
     * @see Filter::setInput()
     */
    public function testSetInputMethodWithBoolValue()
    {
        $class = Filter::class;
        $mock = $this->getMockBuilder($class)
            ->disableOriginalConstructor()
            ->setMethods(['setBoolInput'])
            ->getMock();
        $mock->expects($this->once())
            ->method('setBoolInput')
            ->will($this->returnValue(true));

        $class = new ReflectionClass($class);
        $method = $class->getMethod('setInput');
        $method->setAccessible(true);
        $method->invoke($mock, true);

    }

    /**
     * Test setInput will call setStringInput
     * @see Filter::setInput()
     */
    public function testSetInputMethodWithStringValue()
    {
        $class = Filter::class;
        $mock = $this->getMockBuilder($class)
            ->disableOriginalConstructor()
            ->setMethods(['setStringInput'])
            ->getMock();
        $mock->expects($this->once())
            ->method('setStringInput')
            ->will($this->returnValue(true));

        $class = new ReflectionClass($class);
        $method = $class->getMethod('setInput');
        $method->setAccessible(true);
        $method->invoke($mock, 'like:value');

    }

    /**
     * Test setInput will return instance of Filter::class
     * @see Filter::setInput()
     */
    public function testSetInputMethod()
    {
        $class = new Filter('name', 'value');

        $reflection = new ReflectionClass(Filter::class);
        $method = $reflection->getMethod('setInput');
        $method->setAccessible(true);

        $output = $method->invoke($class, true);

        $this->assertInstanceOf(
            Filter::class,
            $output,
            'Right response type'
        );
    }

    /**
     * @see Filter::setNumericInput()
     */
    public function testSetNumericInputMethod()
    {
        $class = Filter::class;

        $mock = $this->getMockBuilder($class)
            ->disableOriginalConstructor()
            ->getMock();

        $reflection = new ReflectionClass($class);
        $method = $reflection->getMethod('setNumericInput');
        $method->setAccessible(true);

        $output = $method->invoke($mock, 25);

        $this->assertInstanceOf(
            $class,
            $output,
            'Right response type'
        );

        $this->assertAttributeEquals(
            25,
            'value',
            $output,
            'Right attribute value'
        );
        $this->assertAttributeEquals(
            '=',
            'operator',
            $output,
            'Right operator value'
        );

        $output = $method->invoke($mock, '25');
        $this->assertAttributeEquals(
            25,
            'value',
            $output,
            'Right attribute value'
        );

        $output = $method->invoke($mock, 'somestring');
        $this->assertAttributeEquals(
            0,
            'value',
            $output,
            'Right attribute value'
        );
    }

    /**
     * @see Filter::setBoolInput()
     */
    public function testSetBoolInputMethod()
    {
        $class = Filter::class;

        $mock = $this->getMockBuilder($class)
            ->disableOriginalConstructor()
            ->getMock();

        $reflection = new ReflectionClass($class);
        $method = $reflection->getMethod('setBoolInput');
        $method->setAccessible(true);

        $output = $method->invoke($mock, true);

        $this->assertInstanceOf(
            $class,
            $output,
            'Right response type'
        );
        $this->assertAttributeEquals(
            true,
            'value',
            $output,
            'Right attribute value'
        );
        $this->assertAttributeEquals(
            '=',
            'operator',
            $output,
            'Right operator value'
        );

        $output = $method->invoke($mock, false);
        $this->assertAttributeEquals(
            false,
            'value',
            $output,
            'Right attribute value'
        );

        $output = $method->invoke($mock, 'string');
        $this->assertAttributeEquals(
            true,
            'value',
            $output,
            'Right attribute value'
        );
        $output = $method->invoke($mock, ['a', 'r', 'r', 'a', 'y']);
        $this->assertAttributeEquals(
            true,
            'value',
            $output,
            'Right attribute value'
        );
    }

    /**
     * /**
     * Test method when string does not contains : character;
     *      operator=>'=', value=>input
     * @see Filter::setStringInput()
     */
    public function testSetStringInputMethodWithoutOperator()
    {
        $class = Filter::class;

        $mock = $this->getMockBuilder($class)
            ->disableOriginalConstructor()
            ->setMethods(['isValidOperator'])
            ->getMock();

        $mock->expects($this->any())
            ->method('isValidOperator')
            ->will($this->returnCallback(function ($value) {
                return Filter::isOperator($value);
            }));

        $reflection = new ReflectionClass($class);
        $method = $reflection->getMethod('setStringInput');
        $method->setAccessible(true);

        $output = $method->invoke($mock, 'value');

        $this->assertAttributeEquals(
            '=',
            'operator',
            $output,
            'Right operator value'
        );

        $this->assertAttributeEquals(
            'value',
            'value',
            $output,
            'Right attribute value'
        );

        $output = $method->invoke($mock, 'lt');

        $this->assertAttributeEquals(
            'lt',
            'operator',
            $output,
            'Right operator value'
        );

        $this->assertAttributeEquals(
            null,
            'value',
            $output,
            'Right attribute value'
        );
    }

    /**
     * Test method when string contains : character; operator:value
     * @see Filter::setStringInput()
     */
    public function testSetStringInputMethodWithOperator()
    {
        $class = Filter::class;

        $mock = $this->getMockBuilder($class)
            ->disableOriginalConstructor()
            ->setMethods(['isValidOperator'])
            ->getMock();

        $mock->expects($this->any())
            ->method('isValidOperator')
            ->will($this->returnCallback(function ($value) {
                return Filter::isOperator($value);
            }));
        $reflection = new ReflectionClass($class);
        $method = $reflection->getMethod('setStringInput');
        $method->setAccessible(true);

        $output = $method->invoke($mock, 'gt:value');

        $this->assertAttributeEquals(
            'gt',
            'operator',
            $output,
            'Right operator value'
        );

        $this->assertAttributeEquals(
            'value',
            'value',
            $output,
            'Right attribute value'
        );

        $this->expectException(HttpFilterException::class);
        $method->invoke($mock, 'invalid operator:value');

    }

    /**
     * @see Filter::getTarget()
     */
    public function testGetTarget()
    {
        $filter = new Filter('name', 'value');
        $reflection = new ReflectionClass($filter);
        $target = $reflection->getProperty('target');
        $target->setAccessible(true);

        $target->setValue($filter, true);
        $this->assertTrue($filter->getTarget());

        $value = ['a', 'r', 'r', 'a', 'y'];
        $target->setValue($filter, $value);
        $this->assertEquals(
            $value,
            $filter->getTarget(),
            'Right method response'
        );
    }

    /**
     * @see Filter::getInput()
     */
    public function testGetInput()
    {
        $filter = new Filter('name', 'value');
        $reflection = new ReflectionClass($filter);
        $target = $reflection->getProperty('input');
        $target->setAccessible(true);

        $target->setValue($filter, true);
        $this->assertTrue($filter->getInput());

        $value = ['a', 'r', 'r', 'a', 'y'];
        $target->setValue($filter, $value);
        $this->assertEquals(
            $value,
            $filter->getInput(),
            'Right method response'
        );
    }

    /**
     * @see Filter::getValue()
     */
    public function testGetValue()
    {
        $filter = new Filter('name', 'value');
        $reflection = new ReflectionClass($filter);
        $target = $reflection->getProperty('value');
        $target->setAccessible(true);

        $target->setValue($filter, true);
        $this->assertTrue($filter->getValue());

        $value = ['a', 'r', 'r', 'a', 'y'];
        $target->setValue($filter, $value);
        $this->assertEquals(
            $value,
            $filter->getValue(),
            'Right method response'
        );
    }

    /**
     * @see Filter::getOperator()
     */
    public function testGetOperator()
    {
        $filter = new Filter('name', 'value');
        $reflection = new ReflectionClass($filter);
        $target = $reflection->getProperty('operator');
        $target->setAccessible(true);

        $target->setValue($filter, true);
        $this->assertTrue($filter->getOperator());

        $value = ['a', 'r', 'r', 'a', 'y'];
        $target->setValue($filter, $value);
        $this->assertEquals(
            $value,
            $filter->getOperator(),
            'Right method response'
        );
    }
}