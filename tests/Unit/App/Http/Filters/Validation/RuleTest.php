<?php

namespace Tests\Unit\App\Http\Filters\Validation;

use App\Http\Filters\Validation\Rule;
use Tests\TestCase;

/**
 * Class RuleTest
 * @package Tests\Unit\App\Http\Filters\Validation
 */
class RuleTest extends TestCase
{

    /**
     * @see Rule::passes()
     */
    public function testPassesMethod()
    {
        $rule = new Rule();
        $this->assertTrue(
            $rule->passes('field', 'value'),
            'Right output'
        );

        $this->assertFalse(
            $rule->passes('field', 'invalid operator:value'),
            'Right output'
        );

    }

    /**
     * @see Rule::message()
     */
    public function testMessageMethod()
    {
        $rule = new Rule();

        $rule->passes('field', 'invalid operator:value');


        $this->assertEquals(
            'Invalid filter field. Specified value invalid operator:value is not valid',
            $rule->message(),
            'Right message'
        );
    }
}