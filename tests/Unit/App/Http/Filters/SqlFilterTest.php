<?php

namespace Tests\Unit\App\Http\Filters;

use App\Http\Filters\Contracts\HttpFilterContract;
use App\Http\Filters\Filter;
use App\Http\Filters\SqlFilter;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Tests\TestCase;

/**
 * Class SqlFilterTest
 * @package Tests\Unit\App\Http\Filter
 */
class SqlFilterTest extends TestCase
{

    /**
     * Helper method do create mock for different filters
     * @param $operator
     * @return SqlFilter
     */
    protected function operatorTestHelper($operator)
    {
        $mock = $this->getMockBuilder(Filter::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'getOperator'
            ])
            ->getMock();

        $mock->expects($this->once())
            ->method('getOperator')
            ->will($this->returnValue($operator));

        return new SqlFilter($mock);
    }


    /**
     * Helper that mock a filter apply it and return generated query builder
     * @param $operator
     * @param string $field
     * @param string $value
     * @return QueryBuilder
     */
    protected function applyMethodForOperatorTestHelper($operator, $field = 'field', $value = 'value')
    {
        $mock = $this->getMockBuilder(SqlFilter::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'field',
                'operator',
                'value'
            ])
            ->getMock();

        $mock->expects($this->any())
            ->method('field')
            ->will($this->returnValue($field));
        $mock->expects($this->any())
            ->method('operator')
            ->will($this->returnValue($operator));
        $mock->expects($this->any())
            ->method('value')
            ->will($this->returnValue($value));

        $connection = app('db')->connection();
        $query = new QueryBuilder($connection);
        $builder = new EloquentBuilder($query);


        $mock->apply($builder);

        return $builder->getQuery();
    }

    /**
     * @see SqlFilter::__construct()
     */
    public function testConstructMethod()
    {
        $filter = new Filter('target', 'input');

        $sql = new SqlFilter($filter);

        $this->assertAttributeEquals(
            $filter,
            'filter',
            $sql,
            'Right filter instance'
        );
    }

    /**
     * @see SqlFilter::field()
     */
    public function testFieldMethod()
    {
        $filter = new Filter('target', 'input');

        $sql = new SqlFilter($filter);

        $this->assertEquals(
            $filter->getTarget(),
            $sql->field(),
            'Right field'
        );
    }

    /**
     * @see SqlFilter::operator()
     */
    public function testOperatorMethod()
    {

        $helper = function ($operator) {
            $mock = $this->getMockBuilder(Filter::class)
                ->disableOriginalConstructor()
                ->setMethods([
                    'getOperator'
                ])
                ->getMock();

            $mock->expects($this->once())
                ->method('getOperator')
                ->will($this->returnValue($operator));

            $filter = new SqlFilter($mock);
            return $filter->operator();
        };

        $this->assertEquals(
            '<',
            $helper('lt'),
            'Right operator for lt'
        );
        $this->assertEquals(
            '<=',
            $helper('lte'),
            'Right operator for lte'
        );
        $this->assertEquals(
            '>',
            $helper('gt'),
            'Right operator for gt'
        );
        $this->assertEquals(
            '>=',
            $helper('gte'),
            'Right operator for gte'
        );
        $this->assertEquals(
            '=',
            $helper('='),
            'Right operator for ='
        );
        $this->assertEquals(
            'like',
            $helper('like'),
            'Right operator for like'
        );
        $this->assertEquals(
            'in',
            $helper('in'),
            'Right operator for in'
        );
        $this->assertEquals(
            'isnull',
            $helper('isnull'),
            'Right operator for isnull'
        );
        $this->assertEquals(
            'notnull',
            $helper('notnull'),
            'Right operator for notnull'
        );
        $this->assertEquals(
            'default',
            $helper('default'),
            'Right operator for default'
        );
    }

    /**
     * @see SqlFilter::value()
     */
    public function testValueMethod()
    {

        $helper = function ($operator, $value = null, $target = null) {
            $methods = ['getOperator'];
            if (null != $value) {
                $methods[] = 'getValue';
            }
            if (null != $target) {
                $methods[] = 'getTarget';
            }
            $mock = $this->getMockBuilder(Filter::class)
                ->disableOriginalConstructor()
                ->setMethods($methods)
                ->getMock();

            $mock->expects($this->once())
                ->method('getOperator')
                ->will($this->returnValue($operator));
            if (null != $value) {
                $mock->expects($this->once())
                    ->method('getValue')
                    ->will($this->returnValue($value));
            }
            if (null != $target) {
                $mock->expects($this->once())
                    ->method('getTarget')
                    ->will($this->returnValue($target));
            }
            $filter = new SqlFilter($mock);
            return $filter->value();
        };

        $value = 'value';
        $this->assertEquals(
            $value,
            $helper('lt', $value),
            'Right value for operator lt'
        );
        $this->assertEquals(
            $value,
            $helper('lte', $value),
            'Right value for operator lte'
        );
        $this->assertEquals(
            $value,
            $helper('gt', $value),
            'Right value for operator gt'
        );
        $this->assertEquals(
            $value,
            $helper('gte', $value),
            'Right value for operator gte'
        );
        $this->assertEquals(
            $value,
            $helper('=', $value),
            'Right value for operator ='
        );

        $target = 'field';
        $this->assertEquals(
            $target,
            $helper('isnull', null, $target),
            'Right value for operator isnull'
        );
        $this->assertEquals(
            $target,
            $helper('notnull', null, $target),
            'Right value for operator notnull'
        );

        $this->assertEquals(
            '%' . $value . '%',
            $helper('like', $value),
            'Right value for operator like'
        );

        $this->assertEquals(
            [$value],
            $helper('in', $value),
            'Right value for operator in'
        );

        $this->assertEquals(
            ['a', 'b', 'c'],
            $helper('in', 'a,b,c'),
            'Right value for operator in'
        );

        $this->assertEquals(
            $value,
            $helper('limit', $value),
            'Right value for operator limit'
        );
        $this->assertEquals(
            $value,
            $helper('offset', $value),
            'Right value for operator offset'
        );

        $this->assertEquals(
            'desc',
            $helper('sort', '-'),
            'Right value for operator sort'
        );

        $this->assertNull(
            $helper('default'),
            'Right value for operator null'
        );
    }

    /**
     * @see SqlFilter::getOperator()
     * @throws \App\Http\Filters\Exceptions\HttpFilterException
     */
    public function getsGetOperatorMethod()
    {
        $filter = new Filter('target', 'input');

        $sql = new SqlFilter($filter);
        $actual = SqlFilter::getOperator($filter);

        $this->assertEquals(
            $sql->operator(),
            $actual,
            'Right operator'
        );
    }

    /**
     * @see SqlFilter::getValue()
     * @throws \App\Http\Filters\Exceptions\HttpFilterException
     */
    public function getsGetValueMethod()
    {
        $filter = new Filter('target', 'input');

        $sql = new SqlFilter($filter);
        $actual = SqlFilter::getValue($filter);

        $this->assertEquals(
            $sql->value(),
            $actual,
            'Right value'
        );
    }

    /**
     * When operator is [=, <, <=, >, >=,]
     * @see SqlFilter::apply();
     */
    public function testApplyMethodForComparisonOperators()
    {
        $query = $this->applyMethodForOperatorTestHelper('=');
        $this->assertCount(1, $query->wheres);

        $this->assertEquals(
            [
                "type" => "Basic",
                "column" => "field",
                "operator" => "=",
                "value" => "value",
                "boolean" => "and"
            ], $query->wheres[0],
            'Right query values'
        );

    }

    /**
     * When operator is "like"
     * @see SqlFilter::apply();
     */
    public function testApplyMethodForLikeOperators()
    {

        $query = $this->applyMethodForOperatorTestHelper('like', 'field', '%value%');
        $this->assertCount(1, $query->wheres);

        $this->assertEquals(
            [
                "type" => "Basic",
                "column" => "field",
                "operator" => "like",
                "value" => "%value%",
                "boolean" => "and"
            ], $query->wheres[0],
            'Right query values'
        );

    }

    /**
     * When operator is "isnull"
     * @see SqlFilter::apply();
     */
    public function testApplyMethodForIsNullOperators()
    {

        $query = $this->applyMethodForOperatorTestHelper('isnull');
        $this->assertCount(1, $query->wheres);

        $this->assertEquals(
            [
                "type" => "Null",
                "column" => "field",
                "boolean" => "and"
            ], $query->wheres[0],
            'Right query values'
        );

    }

    /**
     * When operator is "notnull"
     * @see SqlFilter::apply();
     */
    public function testApplyMethodForIsNotNullOperators()
    {

        $query = $this->applyMethodForOperatorTestHelper('notnull');
        $this->assertCount(1, $query->wheres);

        $this->assertEquals(
            [
                "type" => "NotNull",
                "column" => "field",
                "boolean" => "and"
            ], $query->wheres[0],
            'Right query values'
        );

    }

    /**
     * When operator is "in"
     * @see SqlFilter::apply();
     */
    public function testApplyMethodForInOperators()
    {

        $query = $this->applyMethodForOperatorTestHelper('in', 'field', ['val1', 'val2']);
        $this->assertCount(1, $query->wheres);

        $this->assertEquals(
            [
                "type" => "In",
                "column" => "field",
                "values" => ["val1", "val2"],
                "boolean" => "and"
            ], $query->wheres[0],
            'Right query values'
        );

    }

    /**
     * When operator is "offset"
     * @see SqlFilter::apply();
     */
    public function testApplyMethodForOffsetOperators()
    {
        $query = $this->applyMethodForOperatorTestHelper('offset', 'field', 2);

        $this->assertEquals(
            2,
            $query->offset,
            'Right offset values'
        );
    }

    /**
     * When operator is "limit"
     * @see SqlFilter::apply();
     */
    public function testApplyMethodForLimitOperators()
    {
        $query = $this->applyMethodForOperatorTestHelper('limit', 'field', 2);

        $this->assertEquals(
            2,
            $query->limit,
            'Right offset values'
        );
    }

    /**
     * When operator is "offset"
     * @see SqlFilter::apply();
     */
    public function testApplyMethodForSortOperators()
    {
        $query = $this->applyMethodForOperatorTestHelper('sort', 'field', 'desc');
        $this->assertCount(1, $query->orders);
        $this->assertEquals(
            [
                'column' => 'field',
                'direction' => 'desc'
            ],
            $query->orders[0],
            'Right sort values'
        );
    }

    /**
     * @see SqlFilter::getOperator()
     */
    public function testGetOperatorMethod()
    {

        $mock = $this->getMockBuilder(HttpFilterContract::class)
            ->setMethods([
                'getOperator',
                'getTarget',
                'getValue'
            ])
            ->getMock();
        $mock->expects($this->once())
            ->method('getOperator')
            ->will($this->returnValue('lt'));

        $this->assertEquals(
            '<',
            SqlFilter::getOperator($mock),
            'Right output value'
        );
    }

    /**
     * @see SqlFilter::getValue()
     */
    public function testGetValueMethod()
    {

        $mock = $this->getMockBuilder(HttpFilterContract::class)
            ->setMethods([
                'getOperator',
                'getTarget',
                'getValue'
            ])
            ->getMock();
        $mock->expects($this->once())
            ->method('getValue')
            ->will($this->returnValue('value'));
        $mock->expects($this->once())
            ->method('getOperator')
            ->will($this->returnValue('='));

        $this->assertEquals(
            'value',
            SqlFilter::getValue($mock),
            'Right output value'
        );
    }

    /**
     * @see SqlFilter::applyToBuilder()
     */
    public function testApplyToBuilderMethod()
    {
        $mock = $this->getMockBuilder(HttpFilterContract::class)
            ->setMethods([
                'getOperator',
                'getTarget',
                'getValue'
            ])
            ->getMock();
        $mock->expects($this->once())
            ->method('getValue')
            ->will($this->returnValue('value'));
        $mock->expects($this->any())
            ->method('getOperator')
            ->will($this->returnValue('='));
        $mock->expects($this->once())
            ->method('getTarget')
            ->will($this->returnValue('field'));

        $connection = app('db')->connection();
        $query = new QueryBuilder($connection);
        $builder = new EloquentBuilder($query);

        $output = SqlFilter::applyToBuilder($builder, $mock);

        $this->assertInstanceOf(
            EloquentBuilder::class,
            $output,
            'Right response type'
        );

        $query = $builder->getQuery();

        $this->assertCount(1, $query->wheres);

        $this->assertEquals(
            [
                "type" => "Basic",
                "column" => "field",
                "operator" => "=",
                "value" => "value",
                "boolean" => "and"
            ], $query->wheres[0],
            'Right query values'
        );

    }

}