<?php

function swg_resource_action($resource)
{
    list($dir, $file) = explode('.', $resource);
    $path = dirname(__FILE__);
    require $path . '/docs/' . $dir . '/' . $file . '.php';
}

swg_resource_action('user.index');
return [
    "openapi" => "3.0.0",
    "info" => [
        "title" => "Account and User Microservice API",
        "version" => "0.1",
    ],
    "servers" => [
        [
            "url" => "http://account.lst",
            "description" => "Account api"
        ]
    ],
    "paths" => [
        "/user" => [
            "description" => "operations for user",
            "summary" => "user crud",
            "get" => swg_resource_action('user.index')

        ],
    ]
];