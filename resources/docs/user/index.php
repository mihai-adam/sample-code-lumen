<?php
return [
    "summary" => "Get a list of users",
    "description" => "Return a paginated list of users",
    "tags" => ["User"],
    "operationId" => "user.index",
    "parameters" => [
        [
            "name" => "sort_by",
            "in" => "query",
            "description" => "Sort by field",
            "schema" => [
                "type" => "string"
            ]
        ]
    ],
    "parameters" => swg_get_params([
        "sort_by" => ["in" => "query", "desc" => "Sort by field"],
        "sort_order" => ["in" => "query", "desc" => "Sort direction"],
        "per_page" => ["in" => "query", "desc" => "Records on page"],
    ]),
    "responses" => [
        "200" => [
            "description" => "Paginated user list",
            "content" => [
                "application/json" => [
                    "schema" => [
                        "type" => "object",
                        "properties" => [
                            "meta" => [
                                "type" => "object",
                                "properties" => [
                                    "current_page" => [
                                        "type" => "integer",
                                        "example" => 1
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]
    ]
];