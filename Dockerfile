FROM php:7.2-fpm

MAINTAINER Mihai Adam <mihai.adam@dynamicart.ro>

WORKDIR /./var/www

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
    curl \
    libmemcached-dev \
    git \
    unzip \
    libz-dev \
    libssl-dev \
    libmcrypt-dev \
    && rm -rf /var/lib/apt/lists/*

  # Install the PHP pdo_mysql extention
RUN docker-php-ext-install pdo_mysql

RUN apt-get update -yqq \
    && pecl channel-update pecl.php.net

RUN pecl install xdebug \
    && docker-php-ext-enable xdebug

RUN printf "\n" | pecl install -o -f redis \
    &&  rm -rf /tmp/pear \
    &&  docker-php-ext-enable redis

  # Install composer
RUN curl -sS https://getcomposer.org/installer | php \
    && mv composer.phar /usr/local/bin/composer