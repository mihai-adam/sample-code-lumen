<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Access extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->getId(),
            'slug' => $this->getSlug(),
            'name' => $this->getName(),
            'description' => $this->getDescription(),
            'enabled' => $this->isEnabled(),
            'enabled_by' => $this->when($this->isEnabled(), $this->enabledBy())
        ];

    }

    protected function enabledBy()
    {
        if ($this->isDisabled()) {
            return false;
        }
        return $this->isEnabledByGroup() ? 'group' : 'action';
    }
}
