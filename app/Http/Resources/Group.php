<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Group extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'account_id' => $this->account_id,
            'name' => $this->name,
            'description' => $this->description
        ];

    }
}
