<?php

namespace App\Http\Resources;

use App\Http\Filters\Standard\FieldsetFilter;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Http\Resources\MergeValue;

class User extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "account_id" => $this->account_id,
            "name" => $this->name,
            "surname" => $this->surname,
            "email" => $this->email,
            "phone" => $this->phone,
            "picture" => $this->picture,
            "created_at" => $this->created_at,
            "confirmed_at" => $this->confirmed_at,
            "updated_at" => $this->updated_at,
            "suspended_at" => $this->suspended_at,
            "deleted_at" => $this->deleted_at,

            "groups" => Group::collection($this->whenLoaded('groups')),
            "actions" => Action::collection($this->whenLoaded('actions')),
            'access' => $this->getAccess($request),
            'account' => new Account($this->whenLoaded('account'))
        ];
    }

    /**
     * Determine what value should have access field
     * @param Request $request
     * @return array|MergeValue|\Illuminate\Support\Collection|mixed
     */
    protected function getAccess(Request $request)
    {
        $filter = new FieldsetFilter($request->get('fieldset'));
        $access = $this->mergeWhen($filter->hasField('access'), Access::collection($this->access()));
        if (is_a($access, MergeValue::class)) {
            $access = $access->data;
        }

        return $access;
    }
}
