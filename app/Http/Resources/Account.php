<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Account extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "picture" => $this->picture,
            "users" => User::collection(($this->whenLoaded('users'))),
            "created_at" => $this->created_at,
            "suspended_at" => $this->suspended_at,
            "updated_at" => $this->updated_at,
        ];
    }
}
