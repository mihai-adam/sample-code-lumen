<?php

namespace App\Http\Filters\Contracts;

interface HttpFilterContract
{

    public function getTarget();

    public function getValue();

    public function getOperator();
}