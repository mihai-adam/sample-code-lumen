<?php

namespace App\Http\Filters\Standard;


use App\Http\Filters\Contracts\HttpFilterContract;

/**
 * Class FieldsetFilter
 * @package App\Http\Filters
 */
class FieldsetFilter implements HttpFilterContract
{

    /**
     * @var array
     */
    protected $value;

    /**
     * FieldsetFilter constructor.
     * @param $value
     */
    public function __construct($value)
    {
        $this->value = explode(',', $value);
    }

    /**
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return null
     */
    public function getTarget()
    {
        return null;
    }

    /**
     * @return string
     */
    public function getOperator()
    {
        return 'with';
    }


    public function hasField($field)
    {
        return in_array($field, $this->value);
    }
}