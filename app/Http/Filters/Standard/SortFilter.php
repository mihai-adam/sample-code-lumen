<?php

namespace App\Http\Filters\Standard;

use App\Http\Filters\Contracts\HttpFilterContract;

/**
 * Class SortFilter
 * @package App\Http\Filters
 */
class SortFilter implements HttpFilterContract
{

    /**
     * @var string
     */
    protected $value;

    /**
     * @var string
     */
    protected $target;

    public function __construct($value)
    {
        if ('-' == substr($value, 0, 1)) {
            $this->value = '-';
            $this->target = substr($value, 1);
        } else {
            $this->value = '+';
            $this->target = $value;
        }
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * @return string
     */
    public function getOperator()
    {
        return 'sort';
    }

}