<?php

namespace App\Http\Filters\Standard;

/**
 * Class OffsetFilter
 * @package App\Http\Filters
 */
class OffsetFilter extends LimitFilter
{

    /**
     * @return string
     */
    public function getOperator()
    {
        return 'offset';
    }
}