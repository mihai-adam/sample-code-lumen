<?php

namespace App\Http\Filters\Standard;


use App\Http\Filters\Contracts\HttpFilterContract;

/**
 * Class LimitFilter
 * @package App\Http\Filters
 */
class LimitFilter implements HttpFilterContract
{

    protected $value;

    public function __construct($value)
    {
        $this->value = intval($value);
    }

    /**
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return null
     */
    public function getTarget()
    {
        return null;
    }

    /**
     * @return string
     */
    public function getOperator()
    {
        return 'limit';
    }
}