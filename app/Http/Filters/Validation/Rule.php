<?php

namespace App\Http\Filters\Validation;

use App\Http\Filters\Exceptions\HttpFilterException;
use App\Http\Filters\Filter;
use App\Http\Filters\FilterAbstract;
use Illuminate\Contracts\Validation\Rule as RuleContract;

/**
 * Class Rule
 * @package App\Http\Filters\Validation
 */
class Rule implements RuleContract
{
    /**
     * @var string
     */
    protected $attribute;

    /**
     * @var string
     */
    protected $value;

    /**
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $this->value = $value;
        $this->attribute = $attribute;
        try {
            new Filter($attribute, $value);
            $output = true;
        } catch (HttpFilterException $exception) {
            $output = false;
        }
        return $output;


    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Invalid filter ' . $this->attribute . '. Specified value ' . $this->value . ' is not valid';
    }
}