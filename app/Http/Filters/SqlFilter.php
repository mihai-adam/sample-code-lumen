<?php

namespace App\Http\Filters;

use App\Http\Filters\Contracts\HttpFilterContract;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class SqlFilter
 * @package App\Http\Filters
 */
class SqlFilter
{
    /**
     * @var HttpFilterContract
     */
    protected $filter;

    /**
     * SqlFilter constructor.
     * @param HttpFilterContract $filter
     */
    public function __construct(HttpFilterContract $filter)
    {
        $this->filter = $filter;
    }

    /**
     * @return mixed
     */
    public function field()
    {
        return $this->filter->getTarget();
    }

    /**
     * @return null|string
     */
    public function operator()
    {
        switch ($operator = $this->filter->getOperator()) {
            case 'lt':
                return '<';
            case 'lte':
                return '<=';
            case 'gt':
                return '>';
            case 'gte':
                return '>=';
            case '=':
                return '=';
            case 'like':
                return 'like';
            case 'in':
                return 'in';
            case 'isnull':
                return 'isnull';
            case 'notnull':
                return 'notnull';
            default:
                return $operator;
        }
    }

    /**
     * @return array|mixed|null|string
     */
    public function value()
    {
        switch ($this->operator()) {
            case '<':
            case '<=':
            case '>':
            case '>=':
            case '=':
                $output = $this->filter->getValue();
                break;
            case 'isnull':
            case 'notnull':
                $output = $this->field();
                break;
            case 'like':
                $output = '%' . $this->filter->getValue() . '%';
                break;
            case 'in':
                $output = explode(',', $this->filter->getValue());
                break;
            case 'limit':
            case 'offset':
                $output = $this->filter->getValue();
                break;
            case 'sort':
                $output = '-' == $this->filter->getValue() ? 'desc' : 'asc';
                break;
            default:
                $output = null;
                break;

        }
        return $output;
    }

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function apply(Builder $builder)
    {
        switch ($operator = $this->operator()) {
            case '<':
            case '<=':
            case '>':
            case '>=':
            case '=':
            case 'like':
                $builder->where($this->field(), $operator, $this->value());
                break;
            case 'isnull':
                $builder->whereNull($this->field());
                break;
            case 'notnull':
                $builder->whereNotNull($this->field());
                break;
            case 'in':
                $builder->whereIn($this->field(), $this->value());
                break;
            case 'offset':
                $builder->offset($this->value());
                break;
            case 'limit':
                $builder->limit($this->value());
                break;
            case 'sort':
                $builder->orderBy($this->field(), $this->value());
                break;
        }
        return $builder;
    }


    /**
     * @param HttpFilterContract $filter
     * @return null|string
     */
    public static function getOperator(HttpFilterContract $filter)
    {
        $instance = new static($filter);
        return $instance->operator();
    }

    /**
     * @param HttpFilterContract $filter
     * @return array|null|string
     */
    public static function getValue(HttpFilterContract $filter)
    {
        $instance = new static($filter);
        return $instance->value();
    }

    /**
     * @param Builder $builder
     * @param HttpFilterContract $filter
     * @return Builder
     */
    public static function applyToBuilder(Builder $builder, HttpFilterContract $filter)
    {

        $instance = new static($filter);
        return $instance->apply($builder);
    }
}