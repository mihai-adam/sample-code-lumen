<?php

namespace App\Http\Filters;

use App\Http\Filters\Contracts\HttpFilterContract;
use App\Http\Filters\Exceptions\HttpFilterException;

/**
 * Class Filter
 * @package App\Http\Filters
 */
class Filter implements HttpFilterContract
{
    /**
     * Available filter operators
     */
    const FILTER_OPERATORS = [
        'lt',
        'lte',
        'gt',
        'gte',
        '=',
        'like',
        'in',
        'isnull',
        'notnull'
    ];
    protected $input;

    /**
     * @var
     */
    protected $target;

    /**
     * @var
     */
    protected $operator;

    /**
     * @var
     */
    protected $value;

    /**
     * SimpleFilter constructor.
     * @param $target
     * @param null $input
     * @throws HttpFilterException
     */
    public function __construct($target, $input = null)
    {
        $this->target = $target;
        $this->input = $input;
        if (null != $input) {
            $this->setInput($input);
        }
    }

    /**
     * @todo add case when array
     * @param $input
     * @return Filter|null
     * @throws HttpFilterException
     */
    protected function setInput($input)
    {
        $output = null;
        if (is_numeric($input)) {
            $output = $this->setNumericInput($input);
        } elseif (is_bool($input)) {
            $output = $this->setBoolInput($input);
        } elseif (is_string($input)) {
            $output = $this->setStringInput($input);
        }
        return $output;
    }

    /**
     * @param $input
     * @return $this
     */
    protected function setNumericInput($input)
    {
        $this->operator = '=';
        $this->value = intval($input);
        return $this;
    }

    /**
     * @param $input
     * @return $this
     */
    protected function setBoolInput($input)
    {
        $this->operator = '=';
        $this->value = boolval($input);
        return $this;
    }

    /**
     * @param $input
     * @return $this
     * @throws HttpFilterException
     */
    protected function setStringInput($input)
    {
        $parts = explode(':', $input);
        if (count($parts) == 2) {
            $operator = $parts[0];
            $value = $parts[1];
            if (!$this->isValidOperator($operator)) {
                $message = 'Unable to parse specified value; specified operator does not exists';
                throw new HttpFilterException($message);
            }
        } elseif (count($parts) == 1) {
            if ($this->isValidOperator($parts[0])) {
                $operator = $parts[0];
                $value = null;
            } else {
                $operator = '=';
                $value = $parts[0];
            }

        } else {
            throw new HttpFilterException('Unable to parse specified value');
        }
        $this->operator = $operator;
        $this->value = $value;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * @return mixed
     */
    public function getOperator()
    {
        return $this->operator;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return null
     */
    public function getInput()
    {
        return $this->input;
    }

    /**
     * Return tru if specified value is a valid aoperator
     * @param $value
     * @return bool
     */
    public static function isOperator($value)
    {
        return in_array($value, self::FILTER_OPERATORS);
    }

    /**
     * @param $value
     * @return bool
     */
    public function isValidOperator($value)
    {
        return static::isOperator($value);
    }
}