<?php

namespace App\Http\Filters;

use App\Http\Filters\Exceptions\HttpFilterException;
use App\Http\Filters\Contracts\HttpFilterContract;
use App\Http\Filters\Standard\FieldsetFilter;
use App\Http\Filters\Standard\LimitFilter;
use App\Http\Filters\Standard\OffsetFilter;
use App\Http\Filters\Standard\SortFilter;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Collection as IlluminateCollection;
use function Psy\debug;

/**
 * Class Manager
 * @package App\Http\Filters
 */
class Manager
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * Fields to be ignored
     * @var array
     */
    protected $disabled = [];

    /**
     * Factory constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }


    /**
     * @return Request|null
     */
    public function request()
    {
        return $this->request;
    }

    /**
     * Add / remove counter specific fields to ignore
     * @param bool $status
     * @return Manager
     */
    public function count($status = true)
    {
        foreach (['sort', 'limit', 'offset'] as $field) {
            if ($status) {
                $this->disable($field);
            } else {
                $this->enable($field);
            }

        }
        return $this;
    }

    /**
     * Remove specified field from disabled fields
     * @param $field
     * @return Manager
     */
    public function enable($field)
    {
        if (($key = array_search($field, $this->disabled)) !== false) {
            unset($this->disabled[$key]);
        }
        return $this;
    }

    /**
     * Add specified field to disabled fields
     * @param $field
     * @return Manager
     */
    public function disable($field)
    {
        if (!in_array($field, $this->disabled)) {
            $this->disabled[] = $field;
        }
        return $this;
    }

    /**
     * List of disabled fields
     * @return array
     */
    public function disabled()
    {
        return $this->disabled;
    }

    /**
     * Remove disabled filters from a list of filters
     * @param $available
     * @return array
     */
    public function enabled($available)
    {
        $names = [];
        foreach ($available as $key => $value) {
            $names[] = is_numeric($key) ? $value : $key;
        }
        return array_values(array_diff($names, $this->disabled));
    }

    /**
     * @param array $filters
     * @return IlluminateCollection
     * @throws HttpFilterException
     */
    public function collect($filters = [])
    {
        $output = new IlluminateCollection();

        foreach ($this->enabled($filters) as $available) {
            $this->request->get($available);
            if (null !== $value = $this->request->get($available)) {
                $parts = array_map('ucfirst', explode('_', $available));
                $method = 'create' . implode('', $parts) . 'Filter';
                if (method_exists($this, $method)) {
                    $filter = $this->$method(['name' => $available, 'value' => $value]);
                } else {
                    try {
                        $filter = $this->makeDefaultFilter($available, $value);
                    } catch (Exception $exception) {
                        throw new HttpFilterException('Unable to create specified filter');
                    }
                }
                if ($filter instanceof HttpFilterContract) {
                    $output->push($filter);
                }

            }
        }
        return $output;
    }

    /**
     * Create a general Filter object
     * @param $name
     * @param $value
     * @return Filter
     * @throws HttpFilterException
     */
    protected function makeDefaultFilter($name, $value)
    {
        return new Filter($name, $value);
    }

    /**
     * Creates Standard Sort Filter
     * @param $data
     * @return SortFilter|null
     */
    protected function createSortFilter($data)
    {
        return (null != $data['value']) ? new SortFilter($data['value']) : null;
    }

    /**
     * Create standard limit filter
     * @param array $data
     * @return LimitFilter|null
     */
    protected function createLimitFilter($data = [])
    {
        return (null != $data['value']) ? new LimitFilter(intval($data['value'])) : null;
    }

    /**
     * Create standard offset filter
     * @param array $data
     * @return OffsetFilter|null
     */
    protected function createOffsetFilter($data = [])
    {
        return (null != $data['value']) ? new OffsetFilter(intval($data['value'])) : null;
    }

    /**
     * @param array $data
     * @return FieldsetFilter|null
     */
    protected function createFieldsetFilter($data = [])
    {
        return (null != $data['value'])
            ? new FieldsetFilter($data['value'])
            : null;
    }


}
