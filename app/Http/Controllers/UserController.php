<?php

namespace App\Http\Controllers;

use App\Http\Requests\Group\UserUpdateRequest;
use App\Http\Requests\Request;
use App\Http\Requests\User\UserCreateRequest;
use App\Http\Requests\User\UserIndexRequest;
use App\Http\Resources\User;
use App\Services\UserService;

/**
 * Class UserController
 * @package App\Http\Controllers
 */
class UserController extends Controller
{

    /**
     * @var UserService
     */
    protected $service;

    /**
     * UserController constructor.
     */
    public function __construct()
    {
        $this->service = new UserService();
    }

    /**
     * @param UserIndexRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(UserIndexRequest $request)
    {
        $query = $this->service
            ->userIndex($request->filters());

        $output = $query->get();

        $resource = User::collection($output);

        return $this->response()->resource($resource, $request);
    }

    /**
     * @param UserIndexRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function count(UserIndexRequest $request)
    {
        $filters = $request->filters()
            ->count();

        $query = $this->service
            ->userIndex($filters);

        $output = $query->count();

        return $this->response()->count($output);
    }

    /**
     * @param $user
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Http\Filters\Exceptions\HttpFilterException
     */
    public function show($user, Request $request)
    {
        $fieldset = $request->filters()
            ->collect(['fieldset'])
            ->first();

        $output = $this->service
            ->userShow($user, $fieldset);

        $resource = new User($output);

        return $this->resource($resource);

    }

    /**
     * @param UserCreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Services\Exceptions\ServiceException
     */
    public function create(UserCreateRequest $request)
    {

        $confirmed = $request->has('confirmed') ? true : false;
        $service = $this->service
            ->userCreate($request->data('data'), $confirmed);

        $resource = $this->shouldResponseItem()
            ? new User($service->getOne())
            : User::collection($service->getCreated());

        return $this->resource($resource, $request, 201);
    }

    /**
     * @param UserUpdateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UserUpdateRequest $request)
    {
        $service = $this->service
            ->userUpdate($request->data('data'), $request->models());

        $resource = $this->shouldResponseItem($request)
            ? new User($service->getOne())
            : User::collection($service->getUpdated());

        return $this->resource($resource, $request, 200);
    }

    public function delete($user)
    {

    }
}