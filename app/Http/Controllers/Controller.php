<?php

namespace App\Http\Controllers;

use Illuminate\Http\Resources\Json\JsonResource;
use Laravel\Lumen\Routing\Controller as BaseController;
use App\Http\Response\Response;
use Illuminate\Http\Request;

/**
 * Class Controller
 * @package App\Http\Controllers
 */
abstract class Controller extends BaseController
{

    const RESOURCE_TYPE_ITEM = 'item';
    const RESOURCE_TYPE_COLLECTION = 'collection';
    const RESOURCE_TYPE_HEADER = 'x-resource-type';

    /**
     * @param null $status
     * @return Response|\Illuminate\Http\JsonResponse
     */
    public function response($status = null)
    {
        $response = new Response();
        return null != $status ? $response->status(intval($status)) : $response;
    }

    /**
     * @param JsonResource $resource
     * @param Request|null $request
     * @param null $status
     * @return \Illuminate\Http\JsonResponse
     */
    protected function resource(JsonResource $resource, Request $request = null, $status = null)
    {
        return $this->response()->resource($resource, $request, $status);
    }

    /**
     * Determine if response should be a resource item
     * @param bool $default
     * @param Request|null $request
     * @return bool
     */
    protected function shouldResponseItem(Request $request = null)
    {
        $request = null != $request ? $request : app('request');
        $header = $request->header(self::RESOURCE_TYPE_HEADER);

        return self::RESOURCE_TYPE_ITEM == $header ? true : false;
    }

}