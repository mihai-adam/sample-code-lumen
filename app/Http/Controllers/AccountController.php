<?php

namespace App\Http\Controllers;

use App\Http\Requests\Account\AccountDeleteRequest;
use App\Http\Requests\Account\AccountIndexRequest;
use App\Http\Requests\Account\AccountCreateRequest;
use App\Http\Requests\Account\AccountUpdateRequest;
use App\Services\AccountService;
use App\Http\Resources\Account;

/**
 * Class AccountController
 * @package App\Http\Controllers
 */
class AccountController extends Controller
{

    /**
     * @var AccountService
     */
    protected $service;

    /**
     * AccountController constructor.
     */
    public function __construct()
    {
        $this->service = new AccountService();
    }

    /**
     * @param AccountIndexRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Http\Filters\Exceptions\HttpFilterException
     */
    public function index(AccountIndexRequest $request)
    {
        $query = $this->service
            ->accountIndex($request->filters());

        $output = $query->get();

        $resource = Account::collection($output);

        return $this->response()->resource($resource, $request);
    }

    /**
     * @param AccountIndexRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Http\Filters\Exceptions\HttpFilterException
     */
    public function count(AccountIndexRequest $request)
    {
        $filters = $request->filters()
            ->count();

        $query = $this->service
            ->accountIndex($filters);

        $output = $query->count();

        return $this->response()->count($output);
    }

    /**
     * @param $account
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($account)
    {
        $output = $this->service
            ->accountShow($account);

        $resource = new Account($output);

        return $this->resource($resource);
    }

    /**
     * @param AccountCreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Services\Exceptions\ServiceException
     */
    public function create(AccountCreateRequest $request)
    {

        $service = $this->service
            ->accountCreate($request->data('data'));

        $resource = $this->shouldResponseItem()
            ? new Account($service->getOne())
            : Account::collection($service->getCreated());

        return $this->resource($resource, $request, 201);
    }

    /**
     * @param AccountUpdateRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Services\Exceptions\ServiceException
     */
    public function update(AccountUpdateRequest $request)
    {
        $service = $this->service
            ->accountUpdate($request->data('data'), $request->models());

        $resource = $this->shouldResponseItem($request)
            ? new Account($service->getOne())
            : Account::collection($service->getUpdated());

        return $this->resource($resource, $request, 200);
    }

    /**
     * @param AccountDeleteRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(AccountDeleteRequest $request)
    {
        $service = $this->service
            ->accountDelete($request->data('data', []));

        return $this->response()->count($service->count());

    }
}