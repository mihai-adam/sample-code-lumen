<?php

namespace App\Http\Middleware;

use Closure;

/**
 * Class ResponseHeadersMiddleware
 * @package App\Http\Middleware
 */
class ResponseHeadersMiddleware
{

    /**
     * Add X-Hostname-Id and X-Request-Id headers to response
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        $response->header('X-Hostname-Id', getenv('HOSTNAME'))
        ->header('X-Request-Id',getenv('X_REQUEST_ID'));

        return $response;
    }
}
