<?php

namespace App\Http\Requests\Account;

use App\Account;
use App\Http\Requests\Request;
use Illuminate\Database\Eloquent\Collection;

class AccountDeleteRequest extends Request
{

    /**
     * @var Collection
     */
    protected $accounts;

    /**
     * @var array
     */
    protected $rules = [
        'data' => "required|array"
    ];

    /**
     * Helper for validation rules
     * @var array
     */
    protected $used = [
        'id' => []
    ];

    /**
     * @return array
     */
    public function rules()
    {
        $this->rules['data.*'] = [

            $this->idExistsRule(),
            $this->idIsNotDuplicatedRule()
        ];

        return $this->rules;
    }

    /**
     * @return Collection
     */
    public function accounts()
    {

        if (null == $this->accounts) {
            $this->accounts = Account::select(['id'])
                ->whereIn('id', $this->data('data', []))
                ->withTrashed()
                ->get(['id']);
        }
        return $this->accounts;
    }


    /**
     * Check if specified id exists in database
     * @return \Closure
     */
    protected function idExistsRule()
    {
        return (function ($attribute, $value, $fail) {
            $account = $this->accounts()->filter(function ($item) use ($value) {
                return $value == $item->id;
            });
            if ($account->isEmpty()) {
                $fail('Specified id: ' . $value . ' does not exists');
            }
        });
    }

    /**
     * Check if specified id has not been specified before;
     * Check if id is not duplicate
     * @return \Closure
     */
    protected function idIsNotDuplicatedRule()
    {
        return (function ($attribute, $value, $fail) {

            if (in_array($value, $this->used['id'])) {
                $fail('Specified id: ' . $value . ' is duplicated');
            } else {
                $this->used['id'][] = $value;
            }
        });
    }
}