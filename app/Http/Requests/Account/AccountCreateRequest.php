<?php

namespace App\Http\Requests\Account;

use App\Http\Requests\Request;

class AccountCreateRequest extends Request
{
    protected $rules = [
        'data' => "required|array",
        'data.*.name' => 'required|string|unique:accounts,name'
    ];
}