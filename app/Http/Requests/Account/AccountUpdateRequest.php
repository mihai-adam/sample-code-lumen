<?php

namespace App\Http\Requests\Account;

use App\Account;
use App\Http\Requests\Standard\UpdateRequest;

class AccountUpdateRequest extends UpdateRequest
{

    protected $rules = [
        'data' => "required|array"
    ];

    /**
     * @return array
     */
    public function rules()
    {
        $this->rules['data.*.id'] = [
            'required',
            $this->idExistsRule(),
            $this->attributeIsNotDuplicatedRule()
        ];
        $this->rules['data.*.name'] = [
            'string',
            'max:64',
            'min:3',
            $this->attributeIsNotDuplicatedRule(),
            $this->attributeIsNotUsedByOtherIdRule()

        ];
        return $this->rules;
    }

    /**
     * @return Account
     */
    protected function model()
    {
        return new Account();
    }


}