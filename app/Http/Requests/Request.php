<?php

namespace App\Http\Requests;

use App\Http\Filters\Manager;
use Illuminate\Http\Request as BaseRequest;
use Illuminate\Config\Repository;
use Illuminate\Contracts\Validation\Factory;
use Illuminate\Validation\ValidatesWhenResolvedTrait;
use Illuminate\Contracts\Validation\ValidatesWhenResolved;


class Request extends BaseRequest implements ValidatesWhenResolved
{

    use ValidatesWhenResolvedTrait {
        prepareForValidation as protected prepareForValidationTrait;
        getValidatorInstance as protected getValidatorInstanceTrait;
    }

    /**
     * @var Repository
     */
    protected $data;

    /**
     * Rules for validation
     * @var array
     */
    protected $rules = [];

    /**
     * Messages for validation errors
     * @var array
     */
    protected $messages = [];


    /**
     * @param null $key
     * @param null $default
     * @return array|mixed|null
     */
    public function data($key = null, $default = null)
    {
        if (null == $this->data) {
            $items = [];
            foreach (array_merge($this->headers->all(), $this->json()->all(), $this->all()) as $name => $value) {
                $items[$name] = $value;
            }
            $this->data = new Repository($items);
        }
        return null != $key ? $this->data->get($key, $default) : $this->data->all();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->rules;
    }

    /**
     * @return array
     */
    public function messages()
    {
        return $this->messages;
    }


    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [];
    }

    /**
     * @return null
     */
    private function prepareForValidation()
    {
        return $this->prepare();
    }

    /**
     * Prepare for validation
     */
    protected function prepare()
    {
        return null;
    }

    /**
     * Get the validator instance for the request.
     *
     * @return \Illuminate\Validation\Validator
     */
    private function getValidatorInstance()
    {
        if (null != $validator = $this->validator()) {
            return $validator;
        }
        $factory = app(Factory::class);

        $validator = $factory->make(
            $this->data(),
            $this->rules(),
            $this->messages(),
            $this->attributes()
        );
        return $validator;
    }

    protected function validator()
    {
        return null;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * @return Manager
     */
    public function filters()
    {
        return new Manager($this);
    }


    /**
     * @param string $name
     * @return mixed|null
     */
    public function __get($name)
    {
        return $this->data->get($name);
    }

    /**
     * Get the route handling the request.
     *
     * @param  string|null $param
     * @param  mixed $default
     * @return \Illuminate\Routing\Route|object|string
     */
    public function route($param = null, $default = null)
    {
        $route = call_user_func($this->getRouteResolver());

        if (is_null($route) || is_null($param)) {
            return $route;
        }
        if (is_null($route) || is_null($param)) {
            return $route;
        } else if (is_array($route)) {

            return (isset($route[2][$param])) ? $route[2][$param] : $default;
        } else {
            return $route->parameter($param);
        }
    }
}