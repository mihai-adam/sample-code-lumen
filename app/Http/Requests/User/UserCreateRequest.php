<?php

namespace App\Http\Requests\User;

use App\Http\Requests\Request;

/**
 * Class UserCreateRequest
 * @package App\Http\Requests\User
 */
class UserCreateRequest extends Request
{

    /**
     * @var array
     */
    protected $rules = [
        'data' => 'required|array',
        'data.*.name' => 'required|string|min:2|max:64',
        'data.*.email' => 'required|email|unique:users,email',
        'data.*.password' => 'required|min:6',
        'data.*.account_id' => 'required|exists:accounts,id',
    ];
}