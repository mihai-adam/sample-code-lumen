<?php

namespace App\Http\Requests\Group;

use App\Account;
use App\Http\Requests\Standard\UpdateRequest;
use App\Invitation;
use App\User;

/**
 * Class UserUpdateRequest
 * @package App\Http\Requests\Group
 */
class UserUpdateRequest extends UpdateRequest
{

    /**
     * @var array
     */
    protected $rules = [
        'data' => "required|array",
        'data.*.name' => 'string|min:2|max:64',
        'data.*.password' => 'min:6'
    ];

    /**
     * @return User|mixed
     */
    protected function model()
    {
        return new User();
    }

    /**
     * @return array
     */
    public function rules()
    {
        $this->rules['data.*.id'] = [
            'required',
            $this->idExistsRule(),
            $this->attributeIsNotDuplicatedRule()
        ];
        $this->rules['data.*.name'] = [
            'string',
            'max:64',
            'min:3'
        ];
        $this->rules['data.*.email'] = [
            'email',
            $this->attributeIsNotUsedByOtherIdRule(),
            $this->notExistsInRule(new Invitation(), 'email')
        ];

        $this->rules['data.*.account_id'] = [
            $this->idExistsInRule(new Account())
        ];
        return $this->rules;
    }
}