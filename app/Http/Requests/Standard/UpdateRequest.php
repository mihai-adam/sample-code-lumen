<?php

namespace App\Http\Requests\Standard;

use App\Http\Requests\Request;
use App\Validation\Contracts\RuleInputContract;
use App\Validation\Contracts\ValidateAgainstModelContract;
use App\Validation\Contracts\ValidateHasInputContract;
use App\Validation\Traits\ValidateAgainstModelRules;
use App\Validation\Traits\ValidateAttributeRules;
use Exception;
use Illuminate\Database\Eloquent\Model;

abstract class UpdateRequest extends Request implements ValidateAgainstModelContract, RuleInputContract
{
    use ValidateAgainstModelRules, ValidateAttributeRules;

    /**
     * @return mixed
     */
    protected abstract function model();

    /**
     * @return array|mixed|null
     */
    public function getValidationInput()
    {
        return $this->data('data', []);
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function getValidationModel()
    {
        $model = $this->model();
        if (!is_a($model, Model::class)) {
            throw new Exception('Model() should ne a instance of eloquent model!');
        }
        return $this->model();
    }


}