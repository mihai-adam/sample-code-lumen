<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Request;

/**
 * Class AuthLoginRequest
 * @package App\Http\Requests\Auth
 */
class AuthLoginRequest extends Request
{
    protected $rules = [

        'email' => "required",
        'password' => "required",
    ];

    /**
     * Get request parameters that are used by validation
     *
     * @return array
     */
    protected function getValidationInput()
    {
        return $this->data();
    }


    public function getUserEmail()
    {
        return $this->data('email');
    }

    public function getUserPassword()
    {
        return $this->data('password');
    }

    public function hasAppKey()
    {
        return null != $this->data('appkey');
    }

    public function appKeyIsValid()
    {
        $key = $this->data('appkey');
        if (null != $key) {
            return in_array($key, app('config')->get('system.appKeys'));
        }
        return false;
    }

}