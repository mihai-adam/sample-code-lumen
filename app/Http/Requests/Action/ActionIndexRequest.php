<?php

namespace App\Http\Requests\Action;

use App\Http\Requests\Request;

class ActionIndexRequest extends Request
{

    protected $filters = [
        'namespace',
        'slug',
        'account_id'
    ];
}