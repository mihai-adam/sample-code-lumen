<?php
/**
 * Created by PhpStorm.
 * User: mihai
 * Date: 20/09/18
 * Time: 11:35
 */

namespace App\Http\Requests\Action;

use App\Action;
use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

class ActionUpdateRequest extends Request
{
    protected $existent;

    protected $rules = [
        'namespace' => "string",
        'description' => "string"
    ];

    public function data($key = null, $default = null)
    {

        return array_merge(parent::data($key = null, $default = null), [
            'slug' => $this->getSlug(),
            'name' => $this->getName()
        ]);
    }


    public function prepareForValidation()
    {
        $this->rules = array_merge($this->rules, [
            'name' => ["string", Rule::unique('actions', 'name')->ignore($this->existent()->name, 'name')],
            'slug' => ['string', Rule::unique('actions', 'slug')->ignore($this->existent()->slug, 'slug')]
        ]);
    }

    public function existent()
    {
        if (null == $this->existent) {
            $this->existent = Action::findOrFail($this->route('action'));
        }
        return $this->existent;
    }


    public function getName()
    {
        return $this->json('name', $this->existent()->name);
    }

    public function getNamespace()
    {
        return $this->json('namespace', $this->existent()->namespace);
    }

    public function getDescription()
    {
        return $this->json('description', $this->existent()->description);
    }

    public function getSlug()
    {
        return str_slug($this->getNamespace() . ' ' . $this->getName());
    }

}