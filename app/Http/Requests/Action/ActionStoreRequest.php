<?php

namespace App\Http\Requests\Action;

use App\Http\Requests\Request;

class ActionStoreRequest extends Request
{
    protected $rules = [
        'name' => "required|string|unique:actions,name",
        'namespace' => "required|string",
        'description' => "required|string"
    ];

    protected $accountId;

    public function getName()
    {
        return $this->data('name');
    }

    public function getNamespace()
    {
        return $this->data('namespace');
    }

    public function getDescription()
    {
        return $this->data('description');
    }

    public function getSlug()
    {
        return str_slug($this->getNamespace() . ' ' . $this->getName());
    }

}