<?php

namespace App\Http\Response;

use Illuminate\Http\Resources\Json\JsonResource;
use Laravel\Lumen\Http\ResponseFactory;
use Illuminate\Http\Request;

/**
 * Class Response
 * @package App\Http\Response
 */
class Response
{

    /**
     * Messages for http statuses
     * @var array
     */
    protected $messages = [
        100 => "Continue",
        101 => "Switching Protocols",
        102 => "Processing",
        200 => "Success",
        201 => "Created",
        202 => "Accepted",
        203 => "Non-Authoritative Information",
        204 => "No Content",
        205 => "Reset Content",
        206 => "Partial Content",
        207 => "Multi-Status",
        300 => "Multiple Choices",
        301 => "Moved Permanently",
        302 => "Found",
        303 => "See Other",
        304 => "Not Modified",
        305 => "Use Proxy",
        306 => "(Unused)",
        307 => "Temporary Redirect",
        308 => "Permanent Redirect",
        400 => "Bad Request",
        401 => "Unauthorized",
        402 => "Payment Required",
        403 => "Forbidden",
        404 => "Not Found",
        405 => "Method Not Allowed",
        406 => "Not Acceptable",
        407 => "Proxy Authentication Required",
        408 => "Request Timeout",
        409 => "Conflict",
        410 => "Gone",
        411 => "Length Required",
        412 => "Precondition Failed",
        413 => "Request Entity Too Large",
        414 => "Request-URI Too Long",
        415 => "Unsupported Media Type",
        416 => "Requested Range Not Satisfiable",
        417 => "Expectation Failed",
        418 => "I'm a teapot",
        419 => "Authentication Timeout",
        420 => "Enhance Your Calm",
        422 => "Unprocessable Entity",
        423 => "Locked",
        424 => "Method Failure",
        425 => "Unordered Collection",
        426 => "Upgrade Required",
        428 => "Precondition Required",
        429 => "Too Many Requests",
        431 => "Request Header Fields Too Large",
        444 => "No Response",
        449 => "Retry With",
        450 => "Blocked by Windows Parental Controls",
        451 => "Unavailable For Legal Reasons",
        494 => "Request Header Too Large",
        495 => "Cert Error",
        496 => "No Cert",
        497 => "HTTP to HTTPS",
        499 => "Client Closed Request",
        500 => "Internal Server Error",
        501 => "Not Implemented",
        502 => "Bad Gateway",
        503 => "Service Unavailable",
        504 => "Gateway Timeout",
        505 => "HTTP Version Not Supported",
        506 => "Variant Also Negotiates",
        507 => "Insufficient Storage",
        508 => "Loop Detected",
        509 => "Bandwidth Limit Exceeded",
        510 => "Not Extended",
        511 => "Network Authentication Required",
        598 => "Network read timeout error",
        599 => "Network connect timeout error"
    ];

    /**
     * @var ResponseFactory
     */
    protected $factory;

    /**
     * Response constructor.
     */
    public function __construct()
    {
        $this->factory = new ResponseFactory;
    }

    /**
     * @param $status
     * @return mixed|string
     */
    protected function message($status)
    {
        return isset($this->messages[$status]) ? $this->messages[$status] : '';
    }

    /**
     * @param $content
     * @param int $status
     * @param array $headers
     * @return \Illuminate\Http\JsonResponse
     */
    public function json($content = null, $status = 200, $headers = [])
    {
        $output = [
            'message' => $this->message($status),
            'code' => $status
        ];
        if (null != $content) {
            $output['data'] = $content;
        }
        return $this->factory->json($output, $status, $headers);
    }

    /**
     * @param $status
     * @return \Illuminate\Http\JsonResponse
     */
    public function status($status)
    {
        return $this->json(null, $status);
    }

    /**
     * @param $count
     * @return \Illuminate\Http\JsonResponse
     */
    public function count($count)
    {
        return $this->json([
            'count' => $count
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function unauthorized()
    {
        return $this->status(401);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function forbidden()
    {
        return $this->status(403);
    }

    /**
     * @param JsonResource $resource
     * @param Request|null $request
     * @param null $status
     * @return \Illuminate\Http\JsonResponse
     */
    public function resource(JsonResource $resource, Request $request = null, $status = null)
    {
        $output = json_decode($resource->response($request)->content(), true);
        $status = null != $status ? $status : 200;
        return $this->json(isset($output['data']) ? $output['data'] : $output, $status);
    }
}