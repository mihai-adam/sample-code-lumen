<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Account
 *
 * Class Account
 *
 * @package App
 *
 * @property string $id System id
 * @property string $name Account name
 * @property string $picture Account picture
 * @property Carbon $created_at Account created at
 * @property Carbon $updated_at Account updated at
 */
class Account extends Model
{
    use SoftDeletes;

    /**
     * @var bool
     */
    protected $hasSysId = true;

    protected $hasBatchId = true;

    /**
     * @var string
     */
    protected $table = 'accounts';

    /**
     * @var bool
     */
    public $incrementing = false;

    protected $dates = ['suspended_at'];

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'picture',
        'suspended_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function groups()
    {
        return $this->hasMany(Group::class);
    }
}