<?php

namespace App\Validation\Traits;


/**
 * Trait ValidatesAgainstModelRules
 * @package App\Validation\Traits
 */
trait ValidateAttributeRules
{

    /**
     * Check if a attribute is duplicated
     * @return \Closure
     */
    protected function attributeIsNotDuplicatedRule()
    {
        return (function ($attribute, $value, $fail) {

            if (!isset($this->cache['values'][$attribute])) {
                $this->cache['values'][$attribute] = [];
            }
            if (in_array($value, $this->cache['values'][$attribute])) {
                $fail('Specified name: ' . $value . ' is duplicated');
            } else {
                $this->cache['values'][$attribute][] = $value;
            }
        });
    }
}