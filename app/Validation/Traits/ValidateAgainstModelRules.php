<?php

namespace App\Validation\Traits;

use App\Model;
use Illuminate\Database\Eloquent\Collection;

/**
 * Trait ValidatesAgainstModelRules
 * @package App\Validation\Traits
 */
trait ValidateAgainstModelRules
{

    /**
     * List of models
     * @var Collection
     */
    private $modelCollection;

    /**
     * @var array
     */
    private $cachedValidationItems = [];

    /**
     * Return a list of items that contains ids specified on @getValidationInput
     * @return Collection
     */
    protected function getValidationCollection()
    {
        if (null == $this->modelCollection) {
            $id = [];
            foreach ($this->getValidationInput() as $row) {
                if (isset($row['id'])) {
                    $id[] = $row['id'];
                }
            }
            $this->modelCollection = $this->getValidationModel()
                ->whereIn('id', $id)
                ->get();
        }
        return $this->modelCollection;
    }

    /**
     * Check if specified id exists in database
     * @return \Closure
     */
    protected function idExistsRule()
    {
        return (function ($attribute, $value, $fail) {
            $model = $this->getValidationCollection()
                ->filter(function ($item) use ($value) {
                    return $value == $item->id;
                });
            if ($model->isEmpty()) {
                $fail('Specified id: ' . $value . ' does not exists');
            }
        });
    }

    /**
     * @param Model $model
     * @return \Closure
     */
    protected function existsInRule(Model $model)
    {
        return (function ($attribute, $value, $fail) use ($model) {
            $model = $this->getIdFromModel($model, $attribute)
                ->filter(function ($item) use ($value) {
                    return $value == $item->id;
                });
            if ($model->isEmpty()) {
                $fail('Specified id: ' . $value . ' does not exists');
            }
        });
    }

    /**
     * Check if a attribute is duplicated
     * @return \Closure
     */
    protected function attributeIsNotUsedByOtherIdRule()
    {
        return (function ($attribute, $value, $fail) {
            if ($this->getAttributesFromDb()->isNotEmpty()) {
                $used = $this->getAttributesFromDb()->filter(function ($item) use ($value) {
                    return $value == $item->name;
                });
                if ($used->isNotEmpty()) {
                    $byInputModel = $this->model()->filter(function ($item) use ($used) {
                        return $used->first()->id == $item->id;
                    });
                    if ($byInputModel->isEmpty()) {
                        $fail('Specified ' . $attribute . ': ' . $value . ' is not available.');
                    }
                }
            }
        });
    }


    /**
     * Check if value does not exists in other tables
     * @todo implement this method
     * @param Model $model
     * @param $attribute
     */
    protected function notExistsInRule(Model $model, $attribute = 'id')
    {

    }


    /**
     * Return a list of id and attributes from db
     * @param $attribute
     * @return mixed
     */
    protected function getAttributesFromDb($attribute)
    {
        if (!isset($this->cachedValidationItems[$attribute])) {

            $attributes = [];
            foreach ($this->getValidationInput() as $row) {
                if (isset($row[$attribute])) {
                    $attributes[] = $row[$attribute];
                }
            }
            $this->cachedValidationItems[$attribute] = $this
                ->getValidationModel()
                ->select(['id', $attribute])
                ->whereIn($attribute, $attributes)
                ->get();
        }
        return $this->cachedValidationItems[$attribute];
    }

    /**
     * Return a list of id and attributes from db
     * @param Model $model
     * @param $key
     * @return mixed
     */
    protected function getIdFromModel(Model $model, $key)
    {
        $cacheKey = '_' . $model->getTable();

        if (!isset($this->cachedValidationItems[$cacheKey])) {

            $attributes = [];
            foreach ($this->getValidationInput() as $row) {
                if (isset($row[$key])) {
                    $attributes = $row[$key];
                }
            }
            $this->cachedValidationItems[$cacheKey] = $model
                ->select(['id'])
                ->whereIn('id', $attributes)
                ->get();
        }
        return $this->cachedValidationItems[$cacheKey];
    }
}