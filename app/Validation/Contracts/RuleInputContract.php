<?php

namespace App\Validation\Contracts;

/**
 * Interface ValidateHasInputContract
 * @package App\Validation\Contracts
 */
interface RuleInputContract
{

    public function getValidationInput();
}