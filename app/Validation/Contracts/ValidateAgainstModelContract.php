<?php

namespace App\Validation\Contracts;

interface ValidateAgainstModelContract
{
    function getValidationModel();

    function getValidationInput();
}