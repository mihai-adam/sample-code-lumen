<?php

namespace App\Utilities\Http;

use Illuminate\Http\Request as BaseRequest;
use App\Utilities\Http\Request\InteractWithFiltersTrait;
use App\Utilities\Http\Request\ValidatesWhenResolvedTrait;
use Illuminate\Contracts\Validation\ValidatesWhenResolved;

/**
 * Class Request
 * @package App\Http\Requests
 *
 * @property array $rules;
 * @property array $messages
 *
 */
class Request extends BaseRequest implements ValidatesWhenResolved
{
    use ValidatesWhenResolvedTrait, InteractWithFiltersTrait;

    const REQUEST_FILTER_PER_PAGE = 'per_page';
    const REQUEST_FILTER_PAGE = 'page';
    const REQUEST_FILTER_SORT_BY = 'sort_by';
    const REQUEST_FILTER_SORT_ORDER = 'sort_order';

    /**
     * Rules for validation
     * @var array
     */
    protected $rules = [];

    /**
     * Messages for validation errors
     * @var array
     */
    protected $messages = [];

    /**
     * Filters that are gonna be used when display a list
     * @var array
     */
    protected $filters = [];

    protected $data;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->rules;
    }

    /**
     * @return array
     */
    public function messages()
    {
        return array_merge($this->messages, [
            'filter' => 'Invalid filter format'
        ]);
    }

    /**
     * @return array
     */
    public function data()
    {
        if (null == $this->data) {
            $this->data = array_merge($this->json()->all(), $this->all());
        }
        return $this->data;
    }

    /**
     * Return a specific value from all input
     * @param $key
     * @param null $default
     * @return mixed|null
     */
    public function any($key, $default = null)
    {
        $data = $this->data();
        return isset($data[$key]) ? $data[$key] : $default;
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [];
    }

    /**
     * Prepare for validation
     */
    protected function prepare()
    {
        return $this;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function filters()
    {
        return array_merge([
            self::REQUEST_FILTER_PER_PAGE,
            self::REQUEST_FILTER_PAGE,
            self::REQUEST_FILTER_SORT_BY,
            self::REQUEST_FILTER_SORT_ORDER,
        ], $this->filters);
    }


    /**
     * @param string $name
     * @return mixed|null
     */
    public function __get($name)
    {
        $data = $this->data();
        if (isset($data[$name])) {
            return $data[$name];
        }
        return null;
    }

    /**
     * Get the route handling the request.
     *
     * @param  string|null $param
     * @param  mixed $default
     * @return \Illuminate\Routing\Route|object|string
     */
    public function route($param = null, $default = null)
    {
        $route = call_user_func($this->getRouteResolver());

        if (is_null($route) || is_null($param)) {
            return $route;
        }
        if (is_null($route) || is_null($param)) {
            return $route;
        } else if (is_array($route)) {

            return (isset($route[2][$param])) ? $route[2][$param] : $default;
        } else {
            return $route->parameter($param);
        }
    }
}