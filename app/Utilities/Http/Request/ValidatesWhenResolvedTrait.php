<?php

namespace App\Utilities\Http\Request;

use Illuminate\Contracts\Validation\Factory as ValidationFactory;
use Illuminate\Validation\ValidationException;
use Illuminate\Validation\Validator;
use Illuminate\Validation\UnauthorizedException;


trait ValidatesWhenResolvedTrait
{

    /**
     * Validate the class instance.
     *
     * @return void
     */
    public function validateResolved()
    {
        $this->prepareForValidation();

        $instance = $this->getValidatorInstance();

        if (!$this->passesAuthorization()) {
            $this->failedAuthorization();
        } elseif (!$instance->passes()) {
            $this->failedValidation($instance);
        }
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        if (method_exists($this, 'prepare')) {
            return $this->prepare();
        }
        return $this;
    }

    /**
     * Get the validator instance for the request.
     *
     * @return \Illuminate\Validation\Validator
     */
    protected function getValidatorInstance()
    {
        if (method_exists($this, 'validator')) {
            return $this->validator();
        }
        $factory = app(ValidationFactory::class);
        $validator = $factory->make(
            $this->validationData(),
            $this->validationRules(),
            $this->validationMessages(),
            $this->validationAttributes()
        );
        return $validator;
    }

    /**
     * @return array
     */
    protected function validationData()
    {
        if (method_exists($this, 'data')) {
            return $this->data();
        }
        return [];
    }

    /**
     * @return array
     */
    protected function validationRules()
    {
        if (method_exists($this, 'rules')) {
            return $this->rules();
        }
        return [];
    }


    /**
     * @return array
     */
    protected function validationMessages()
    {
        if (method_exists($this, 'messages')) {
            return $this->messages();
        }
        return [];
    }

    /**
     * @return array
     */
    protected function validationAttributes()
    {
        if (method_exists($this, 'attributes')) {
            return $this->attributes();
        }
        return [];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Validation\Validator $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        throw new ValidationException($validator);
    }

    /**
     * Determine if the request passes the authorization check.
     *
     * @return bool
     */
    protected function passesAuthorization()
    {
        if (method_exists($this, 'authorize')) {
            return $this->authorize();
        }
        return true;
    }

    /**
     * Handle a failed authorization attempt.
     *
     * @return void
     *
     * @throws \Illuminate\Validation\UnauthorizedException
     */
    protected function failedAuthorization()
    {
        throw new UnauthorizedException;
    }
}