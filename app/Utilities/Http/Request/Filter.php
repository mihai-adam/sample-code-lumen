<?php

namespace App\Utilities\Http\Request;

use Illuminate\Database\Eloquent\Builder;

/**
 * Class Filter
 * @package App\Utilities\Http\Request
 */
class Filter
{

    /**
     * @var
     */
    protected $name;

    /**
     * @var
     */
    protected $operator;

    /**
     * @var
     */
    protected $value;

    /**
     * @var
     */
    protected $column;

    /**
     * Filter constructor.
     * @param $name
     * @param null $value
     */
    public function __construct($name, $value = null)
    {
        $this->name = $name;
        if (null != $value) {
            $this->setValue($value);
        }
    }

    public function getName()
    {
        return $this->name;
    }

    public function getValue($converted = false)
    {
        return ($converted) ? $this->getSqlConvertedValue() : $this->value;
    }

    public function getColumn()
    {
        return $this->column;
    }

    public function getOperator($converted = false)
    {

        return ($converted) ? $this->getSqlConvertedOperator() : $this->operator;
    }


    public function getSqlConvertedValue()
    {
        switch ($this->getSqlConvertedOperator()) {
            case '<':
            case '<=':
            case '>':
            case '>=':
            case '=':
            case 'isnull':
            case 'isnotnull':
                $output = (null != $this->column) ? $this->column : $this->value;
                break;
            case 'like':
                $output = '%' . $this->value . '%';
                break;
            case 'in':
                $output = explode(',', $this->value);
                break;

            default:
                $output = null;
        }
        return $output;
    }

    public function getSqlConvertedOperator()
    {
        return self::convertToSqlOperator($this->operator);
    }

    /**
     * @param $value
     */
    public function setValue($value)
    {
        $parts = self::parseValue($value);
        $this->operator = $parts['operator'];
        $this->value = $parts['value'];
        $this->column = $parts['column'];
    }

    /**
     * @param $value
     * @return bool
     */
    public static function hasValidValue($value)
    {
        $parsed = self::parseValue($value);

        return null != $parsed['operator'];
    }

    /**
     * @param $value
     * @return array
     */
    public static function parseValue($value)
    {
        if (is_numeric($value)) {
            $output = [
                'operator' => '=',
                'value' => $value,
                'column' => null
            ];
        } elseif (is_bool($value)) {
            $output = [
                'operator' => null,
                'value' => $value,
                'column' => null
            ];
        } elseif (is_string($value)) {
            $output = self::parseStringValue($value);
        } elseif (is_array($value)) {
            $output = self::parseArrayValue($value);
        } else {
            $output = [
                'operator' => null,
                'value' => null,
                'column' => null
            ];
        }
        if (null == self::convertToSqlOperator($output['operator'])) {
            $output['operator'] = null;
        }
        return $output;
    }

    /**
     * @param $value
     * @return array
     */
    public static function parseStringValue($value)
    {
        $parts = explode(':', $value);
        if (count($parts) == 2) {
            $operator = $parts[0];
            $value = $parts[1];
        } elseif (count($parts) == 1) {
            $operator = '=';
            $value = $parts[0];
        } else {
            $value = null;
            $operator = null;
        }
//          this supposed to kelp with filters but cause errors when email field is involved
//        $parts = explode('.', $value);
//        if (2 == count($parts)) {
//            $column = $parts[0];
//            unset($value);
//        }
        return [
            'operator' => isset($operator) ? $operator : null,
            'value' => isset($value) ? $value : null,
            'column' => isset($column) ? $column : null
        ];
    }

    /**
     * @param $operator
     * @return null|string
     */
    protected static function convertToSqlOperator($operator)
    {
        switch ($operator) {
            case 'lt':
                return '<';
            case 'lte':
                return '<=';
            case 'gt':
                return '>';
            case 'gte':
                return '>=';
            case '=':
                return '=';
            case 'like':
                return 'like';
            case 'in':
                return 'in';
            case 'isnull':
                return 'isnull';
            default:
                return null;
        }
    }


    public function applyToQuery(Builder $builder, $column = null)
    {
        $column = null != $column ? $column : $this->name;
        $operator = $this->getSqlConvertedOperator();
        $value = $this->getSqlConvertedValue();
        switch ($operator) {
            case '<':
            case '<=':
            case '>':
            case '>=':
            case '=':
            case 'like':
                $builder->where($column, $operator, $value);
                break;
            case 'isnull':
                $builder->whereNull($column);
            case 'isnotnull':
                $builder->whereNotNull($column);
                break;
            case 'in':
                $builder->whereIn($column, $value);
                break;
        }
        return $builder;

    }
}