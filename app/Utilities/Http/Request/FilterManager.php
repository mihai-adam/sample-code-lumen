<?php
/**
 * Created by PhpStorm.
 * User: mihai
 * Date: 20.02.2018
 * Time: 11:43
 */

namespace App\Utilities\Http\Request;


use Illuminate\Support\Collection;

class FilterManager
{

    protected $filters;

    protected $sortBy = 'created_at';
    protected $sortOrder = 'DESC';
    protected $perPage = 25;

    public function __construct()
    {
        $this->filters = new Collection();
    }

    public function add(Filter $filter)
    {
        $this->filters->push($filter);
        return $this;
    }

    public function getFilters()
    {
        return $this->filters;
    }


    public function hasFilter($name)
    {
        return null != $this->getFilter($name);
    }

    public function getFilter($name)
    {
        return $this->getFilters()->first(function ($item) use ($name) {
            return $item->getName() == $name;
        });
    }

    public function getFilterValue($name, $default = null)
    {
        return ($this->hasFilter($name)) ?
            $this->getFilter($name)->getSqlConvertedValue() :
            $default;
    }

    public function setDefaultSortBy($value)
    {
        $this->sortBy = $value;
        return $this;
    }

    public function getSortBy()
    {
        return $this->getFilterValue('sort_by', $this->sortBy);
    }


    public function setDefaultSortOrder($value)
    {
        $this->sortOrder = $value;
        return $this;
    }

    public function getSortOrder()
    {
        return $this->getFilterValue('sort_order', $this->sortOrder);
    }

    public function getPage()
    {
        return $this->getFilterValue('page', 1);
    }

    public function getPerPage()
    {
        return $this->getFilterValue('per_page', $this->perPage);
    }

    public function __get($name)
    {
        if ($this->hasFilter($name)) {
            return $this->getFilter($name);
        }
        return null;

    }
}