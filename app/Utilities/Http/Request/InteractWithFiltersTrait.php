<?php

namespace App\Utilities\Http\Request;

/**
 * Trait InteractWithFiltersTrait
 * @package App\Utilities\Http\Request
 */
trait InteractWithFiltersTrait
{

    /**
     * @var FilterManager
     */
    protected $filterManager;

    /**
     * @return FilterManager
     */
    public function getFilterManager()
    {
        if (null == $this->filterManager) {
            $manager = new FilterManager();

            foreach ($this->getAvailableInputFilters() as $name => $filter) {
                $manager->add($this->makeRequestFilter($name, $filter));
            }
            $this->filterManager = $manager;
        }
        return $this->filterManager;
    }

    /**
     * @return array
     */
    protected function getAvailableInputFilters()
    {
        $output = [];
        $available = [];
        if (method_exists($this, 'filters')) {
            $available = $this->filters();
        }

        if (method_exists($this, 'data')) {
            $input = $this->data();
        } elseif (method_exists($this, 'input')) {
            $input = $this->input();
        } else {
            $input = [];
        }
        foreach ($input as $key => $value) {
            if (in_array($key, $available)) {
                $output[$key] = $value;
            }
        }
        return $output;
    }

    /**
     * @param $name
     * @param $filter
     * @return Filter
     */
    protected function makeRequestFilter($name, $filter)
    {
        $parts = explode('_', $name);
        array_walk($parts, function (&$value) {
            $value = ucfirst(strtolower($value));
        });
        $method = 'make' . implode(',', $parts) . 'Filter';
        if (method_exists($this, $method)) {
            return $this->{$method}($filter);
        }
        return $this->makeDefaultFilter($name, $filter);
    }

    /**
     * @param $name
     * @param $value
     * @return Filter
     */
    protected function makeDefaultFilter($name, $value)
    {
        $filter = new Filter($name, $value);

        return $filter;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getFilters()
    {
        return $this->getFilterManager()->getFilters();
    }
}