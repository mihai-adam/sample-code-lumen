<?php

namespace App;

use Carbon\Carbon;

/**
 * App\Group
 *
 * Class Group
 *
 * @package App
 *
 * @property string $id
 * @property string $account_id
 * @property string $name
 * @property string $description
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Group extends Model
{
    /**
     * @var bool
     */
    protected $hasSysId = true;

    /**
     * @var bool
     */
    protected $hasBatchId = true;

    /**
     * @var string
     */
    protected $table = 'groups';

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function account()
    {
        return $this->belongsTo(Account::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function actions()
    {
        return $this->belongsToMany(Action::class, 'group_actions');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'user_groups');
    }


}