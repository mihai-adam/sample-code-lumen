<?php

namespace App;

use App\Services\UserService;
use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

/**
 * App\User
 *
 * Class User
 *
 * @package App
 *
 * @property integer $id
 * @property string $sys_id
 * @property integer $account_id
 * @property string $name
 * @property string $surname
 * @property string $password
 * @property string $picture
 * @property bool $is_admin
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $suspended_at
 * @property \Carbon\Carbon $deleted_at
 *
 */
class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'account_id',
        'name',
        'surname',
        'email',
        'password',
        'picture'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * @var bool
     */
    protected $hasSysId = true;

    /**
     * @var bool
     */
    protected $hasBatchId=true;

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'confirmed_at',
        'updated_at',
        'suspended_at',
        'deleted_at'
    ];

    /**
     * Encrypt password
     * @param $value
     * @return $this
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = app('hash')->make($value);
        return $this;
    }


    public function scopeActive(Builder $query)
    {
        return $query->whereNull('suspended_at')
            ->whereNull('deleted_at');
    }

    public function scopeConfirmed(Builder $query)
    {
        return $query->whereNotNull('confirmed_at');
    }

    public function groups()
    {
        return $this->belongsToMany(Group::class, 'user_groups');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function actions()
    {
        return $this->belongsToMany(Action::class, 'user_actions');
    }

    /**
     * User Access
     * @return array
     */
    public function access()
    {
        return UserService::userAccess($this);
    }

    /**
     * User Access
     * @return array
     */
    public function account()
    {
        return $this->belongsTo(Account::class);
    }
}
