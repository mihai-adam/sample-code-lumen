<?php

namespace App\Providers;

use App\Http\Filters\AbstractFilter;
use Illuminate\Http\Request as IlluminateRequest;
use App\Http\Requests\Request as AppRequest;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use Illuminate\Contracts\Validation\ValidatesWhenResolved;

/**
 * Class RequestServiceProvider
 * @package App\Providers
 */
class RequestServiceProvider extends ServiceProvider
{

    /**
     * Boot application services.
     *
     * @return void
     */
    public function boot()
    {

        $this->app->resolving(AppRequest::class, function ($request, $app) {
            $this->bootRequest($request, $app['request']);
        });

        $this->app->afterResolving(ValidatesWhenResolved::class, function ($resolved) {
            $resolved->validateResolved();
        });

    }

    /**
     * @param AppRequest $form
     * @param IlluminateRequest $current
     */
    protected function bootRequest(AppRequest $form, IlluminateRequest $current)
    {
        $files = $current->files->all();

        $files = is_array($files) ? array_filter($files) : $files;

        $form->initialize(
            $current->query->all(), $current->request->all(), $current->attributes->all(),
            $current->cookies->all(), $files, $current->server->all(), $current->getContent()
        );

        $form->setJson($current->json());

        if ($session = $current->getSession()) {
            $form->setLaravelSession($session);
        }

        $form->setUserResolver($current->getUserResolver());

        $form->setRouteResolver($current->getRouteResolver());
    }
}
