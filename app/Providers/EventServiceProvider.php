<?php

namespace App\Providers;

use Dingo\Api\Event\ResponseWasMorphed;
use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;
use Utilities\Event\Handler\DingoCorsResponse;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        ResponseWasMorphed::class => [
            DingoCorsResponse::class,
        ],
    ];
}
