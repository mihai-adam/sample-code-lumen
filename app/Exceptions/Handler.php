<?php

namespace App\Exceptions;


use Barryvdh\Cors\CorsService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\HttpKernel\Exception\HttpException;

use Symfony\Component\Debug\ExceptionHandler as SymfonyExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if ($e instanceof ApiHttpException) {
            return $e->getBody();
        } elseif ($e instanceof HttpResponseException) {
            return $e->getResponse();
        } elseif ($e instanceof ModelNotFoundException) {
            return response([
                'status' => 'error',
                'message' => $e->getMessage()
            ], 404);
        } elseif ($e instanceof AuthorizationException) {
            $e = new HttpException(403, $e->getMessage());
        } elseif ($e instanceof ValidationException && $e->getResponse()) {
            return $e->getResponse();
        } elseif ($e instanceof ValidationException) {
            return response([
                'status' => 'error',
                'message' => $e->getMessage(),
                'errors' => $e->errors()
            ], $e->status);
        } elseif ($e instanceof NotFoundHttpException) {
            $e = new NotFoundHttpException('page not found', $e);
        }
        $fe = FlattenException::create($e);
        $output = [
            'status' => 'error',
            'code' => $fe->getStatusCode(),
            'message' => $fe->getMessage(),
            'meta' => $fe->toArray()[0]
        ];
        $response = new JsonResponse($output, $fe->getStatusCode(), $fe->getHeaders());
        $response->exception = $e;

        return $response;

    }
}
