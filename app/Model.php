<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model as BaseModel;

/**
 * App\Model
 *
 * Class Model
 *
 * @package App
 *
 */
abstract class Model extends BaseModel
{

    protected $hasSysId = false;

    protected $hasBatchId = false;


    /**
     * Generate a unique id
     * @return string
     */
    public static function generateSysId()
    {
        return config('system.ms_key') . '_' . bin2hex(openssl_random_pseudo_bytes(8));
    }

    /**
     * Generate unique batch_id
     * @return string
     */
    public static function generateBatchId()
    {
        return bin2hex(openssl_random_pseudo_bytes(8));
    }

    /**
     * Perform a model insert operation.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @return bool
     */
    protected function performInsert(Builder $query)
    {
        if ($this->hasSysId) {
            $this->setAttribute('id', self::generateSysId());
        }
        if ($this->hasBatchId) {
            $this->setAttribute('batch_id', self::generateBatchId());
        }
        return parent::performInsert($query);
    }
}