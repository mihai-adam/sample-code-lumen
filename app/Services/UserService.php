<?php

namespace App\Services;

use App\Group;
use App\Http\Filters\Manager;
use App\Http\Filters\SqlFilter;
use App\Http\Filters\Standard\FieldsetFilter;
use App\User;
use App\Services\Users\UserDeleteService;
use App\Services\Users\UserCreateService;
use App\Services\Users\UserUpdateService;
use App\Services\Users\UserAccessService;

/**
 * Class UserService
 * @package App\Services
 */
class UserService
{


    public function userIndex(Manager $manager)
    {
        $query = User::select();
        $filters = $manager->collect([
            'sort',
            'limit',
            'offset',
            'account_id',
            'name',
            'email',
            'phone',
            'picture'
        ]);
        $filters->each(function ($filter) use ($query) {
            SqlFilter::applyToBuilder($query, $filter);
        });
        return $query;
    }

    /**
     * @param Manager $manager
     * @return mixed
     */
    public function userIndexOld(Manager $manager)
    {
        if (null != $closure) {
            $closure($this);
        }
        $query = User::select()
            ->with([
                'groups',
                'actions',
                'account'
            ]);

        if ($manager->id) {
            $manager->id->applyToQuery($query);
        }

        if ($manager->account_id) {
            $manager->account_id->applyToQuery($query);
        }

        if ($manager->group_id) {
            $filter = $manager->group_id;

            $query->whereHas('groups', function ($q) use ($filter) {
                return $q->where('id', $filter->getOperator(true), $filter->getValue(true));
            });
        }

        if ($manager->action_id) {
            $filter = $manager->action_id;
            $query->whereHas('actions', function ($q) use ($filter) {
                return $q->where('id', $filter->getOperator(true), $filter->getValue(true));
            });
        }

        if ($manager->email) {
            $manager->email->applyToQuery($query);
        }
        return $query->paginate($manager->getPerPage());
    }

    /**
     * @param $data
     * @param bool $confirmed
     * @return UserCreateService
     */
    public function userCreate($data, $confirmed = false)
    {
        $service = new UserCreateService($confirmed);
        return $service->run($data);
    }

    public function userShow($id, FieldsetFilter $fieldset = null)
    {
        $with = null != $fieldset
            ? array_intersect(['groups', 'account'], $fieldset->getValue())
            : [];
        return User::with($with)->findOrFail($id);
    }

    /**
     * @param $id
     * @param $params
     * @return UserUpdateService
     */
    public function userUpdate($id, $params)
    {
        $model = User::findOrFail($id);
        return new UserUpdateService($model, $params);
    }

    /**
     * @param $ids
     * @return UserDeleteService
     */
    public function userDelete($ids)
    {
        $service = new UserDeleteService();
        $collection = User::whereIn('id', $ids)->get();
        $service->setCollection($collection);
        return $service;
    }

    /**
     * @param $email
     * @param $password
     * @return bool
     */
    public function login($email, $password)
    {
        $user = User::where('email', $email)
            ->with([
                'groups',
                'actions'
            ])
            ->active()
            ->first();
        if ($user) {
            if (app('hash')->check($password, $user->getAuthPassword())) {
                return $user;
            }
        }
        return false;
    }

    /**
     * Calculate user access
     * @param User $user
     * @return array
     */
    public static function userAccess(User $user)
    {
        return UserAccessService::factory($user)->run();

    }

    public function userSyncGroups($userIds, $groupIds, $accountId = null)
    {
        $query = User::whereIn('id', $userIds);
        if ($accountId) {
            $query->where('account_id', $accountId);
        }
        $users = $query->get();

        $query = Group::whereIn('id', $groupIds);
        if ($accountId) {
            $query->where('account_id', $accountId);
        }
        $groups = $query->get()->pluck('id')->toArray();

        foreach ($users as $user) {
            $user->groups()->sync($groups);
        }
        return User::whereIn('id', $userIds)->with('groups')->get();


    }


}