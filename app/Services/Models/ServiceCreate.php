<?php

namespace App\Services\Models;

use App\Model;
use App\Services\Exceptions\ServiceException;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Collection as IlluminateCollection;

/**
 * Class ModelCreateService
 * @package App\Services
 */
abstract class ServiceCreate
{

    /**
     * @var string
     */
    protected $batchId;

    /**
     * @var
     */
    protected $model;

    /**
     * @var Carbon
     */
    protected $now;

    /**
     * Get a new model object
     * @return Model
     */
    protected abstract function model();

    /**
     * Prepare a row for insert
     * @param $data
     * @return mixed
     */
    protected abstract function row($data);

    /**
     * Perform the insert
     * @param array $data
     * @return $this
     */
    public function run($data = [])
    {
        $source = new IlluminateCollection();
        foreach ($data as $record) {
            $source->push($this->row($record));
        }

        $this->beforeCreated($source);

        $this->model()->insert($source->toArray());
        $this->afterCreated();
        return $this;
    }


    /**
     * @return Carbon|string
     */
    protected function now()
    {
        if (null == $this->now) {
            $this->now = Carbon::now('utc');
        }
        return $this->now;
    }

    /**
     * generate id in order to perform an insert
     * @return string
     */
    protected function id()
    {
        return Model::generateSysId();
    }

    /**
     * @return string
     */
    public function batchId()
    {
        if (null == $this->batchId) {
            $this->batchId = Model::generateBatchId();
        }
        return $this->batchId;
    }


    /**
     * Collection of new records
     * @return Collection
     */
    public function getCreated()
    {
        return $this->model()
            ->where('batch_id', $this->batchId())
            ->get();
    }

    /**
     * Attempt to pick one record
     * @return mixed
     * @throws ServiceException
     */
    public function getOne()
    {
        $created = $this->getCreated();
        if (1 == $created->count()) {
            return $created[0];
        } else {
            $message = 'Unable to get one account; ' . $created->count() . ' created';
            throw new ServiceException($message);
        }
    }

    /**
     * Return all created records
     * @return Collection
     */
    public function getAll()
    {
        return $this->getCreated();
    }

    /**
     * @param $source
     * @return mixed
     */
    protected function beforeCreated($source)
    {
        return $source;
    }

    /**
     * Executed after all records ware inserted
     */
    protected function afterCreated()
    {
        return $this;
    }
}