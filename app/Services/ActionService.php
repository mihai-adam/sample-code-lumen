<?php

namespace App\Services;

use App\Action;
use App\Utilities\Http\Request\FilterManager;

/**
 * Class ActionService
 * @package App\Services
 */
class ActionService
{
    /**
     * @param FilterManager $filters
     * @return mixed
     */
    public function actionIndex(FilterManager $filters)
    {

        $filters->setDefaultSortBy('slug')
            ->setDefaultSortOrder('ASC');

        $query = Action::select()
            ->orderBy($filters->getSortBy(), $filters->getSortOrder());

        if ($filters->hasFilter('account_id')) {
            $filter = $filters->getFilter('account_id');
            $query->with(['groups' => function ($q) use ($filter) {
                return $q->where('account_id', $filter->getOperator(), $filter->getValue());
            }]);
        } else {
            $query->with(['groups']);
        }
        if ($filters->hasFilter('namespace')) {
            $filter = $filters->getFilter('namespace');
            $query->where('namespace', $filter->getOperator(), $filter->getValue(true));
        }
        return $query->paginate($filters->getPerPage());
    }

    /**
     * @param $name
     * @param $namespace
     * @param $description
     * @param $slug
     * @return Action
     */
    public function actionStore($name, $namespace, $description, $slug)
    {
        $action = new Action();
        $action->name = $name;
        $action->namespace = $namespace;
        $action->description = $description;
        $action->slug = $slug;
        $action->save();
        return $action;
    }

    public function actionShow($id)
    {
        return Action::findOrFail($id);
    }


    public function actionUpdate($id, $data)
    {
        $action = Action::findOrFail($id);
        $action->update($data);
        return $action;
    }

    public function actionDelete($id)
    {
        $action = Action::findOrFail($id);
        $action->groups()->detach();
        return $action->delete();
    }
}