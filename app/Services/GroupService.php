<?php

namespace App\Services;

use App\User;
use App\Group;
use App\Action;
use App\Transformers\GroupTransformer;
use App\Services\Groups\GroupIndexService;
use App\Services\Groups\GroupUsersService;
use App\Services\Groups\GroupCreateService;
use App\Services\Groups\GroupDeleteService;
use App\Services\Groups\GroupUpdateService;
use App\Services\Groups\GroupActionsService;
use App\Services\Groups\GroupAttachUsersService;
use App\Utilities\Http\Request\FilterManager;
use Illuminate\Support\Collection;

/**
 * Class GroupService
 * @package App\Services
 */
class GroupService
{
    /**
     * @param FilterManager $filters
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function groupIndex(FilterManager $filters)
    {
        $query = Group::with(['actions'])
            ->orderBy($filters->getSortBy(), $filters->getSortOrder());

        if ($filters->hasFilter('account_id')) {
            $filter = $filters->getFilter('account_id');
            $query->where('account_id', $filter->getOperator(), $filter->getValue(true));
        }

        $data = $query->paginate($filters->getPerPage());

        return $data;
    }

    /**
     * @return GroupCreateService
     */
    public function groupCreate()
    {
        return new GroupCreateService();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function groupShow($id)
    {
        return Group::with(['actions', 'users'])->findOrFail($id);
    }

    /**
     * @param $id
     * @return GroupUpdateService
     */
    public function groupUpdate($id)
    {
        $model = Group::findOrFail($id);
        return new GroupUpdateService($model);
    }

    /**
     * @param array $groupIds
     * @param string $accountId
     * @return bool
     */
    public function groupDelete($groupIds = [], $accountId = null)
    {
        $query = Group::whereIn('id', $groupIds);
        if (null != $accountId) {
            $query->where('account_id', $accountId);
        }
        $groups = $query->get();
        if (!$groups->isEmpty()) {
            $groups->each(function ($g) {
                $g->actions()->detach();
                $g->delete();
            });
        }
        return true;
    }

    /**
     * @param $groupIds
     * @param $userIds
     * @param null $accountId
     * @return Group[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function groupAttachUsers($groupIds, $userIds, $accountId = null)
    {

        $query = Group::with(['users' => function ($q) use ($userIds, $accountId) {
            $q->whereIn('id', $userIds);
            if (null != $accountId) {
                $q->where('account_id', $accountId);
            }
            return $q;
        }])->whereIn('id', $groupIds);
        if (null != $accountId) {
            $query->where('account_id', $accountId);
        }
        $groups = $query->get();

        foreach ($groups as $group) {
            foreach ($userIds as $userId) {
                $user = $group->users->first(function ($value) use ($userId) {
                    return $value->id == $userId;
                });
                if (null == $user) {
                    $group->users()->attach($userId);
                }
            }
            $group->load(['users' => function ($q) use ($userIds, $accountId) {
                $q->whereIn('id', $userIds);
                if (null != $accountId) {
                    $q->where('account_id', $accountId);
                }
                return $q;
            }]);
        }
        return $groups;
    }

    /**
     * @param $groupIds
     * @param $userIds
     * @param $accountId
     * @return Group[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function groupDetachUsers($groupIds, $userIds, $accountId = null)
    {
        $query = Group::with(['users' => function ($q) use ($userIds, $accountId) {
            $q->whereIn('id', $userIds);
            if (null != $accountId) {
                $q->where('account_id', $accountId);
            }
            return $q;
        }])->whereIn('id', $groupIds);

        if (null != $accountId) {
            $query->where('account_id', $accountId);
        }
        $groups = $query->get();

        foreach ($groups as $group) {
            foreach ($userIds as $userId) {
                $user = $group->users->first(function ($value) use ($userId) {
                    return $value->id == $userId;
                });
                if (null != $user) {
                    $group->users()->detach($userId);
                }
            }
            $group->load(['users' => function ($q) use ($accountId) {
                if (null != $accountId) {
                    $q->where('account_id', $accountId);
                }
                return $q;
            }]);
        }
        return $groups;
    }

    public function groupSyncUsers($groupIds, $userIds, $accountId = null){

    }

    /**
     * @param $groupIds
     * @param $actionIds
     * @param null $accountId
     * @return Group[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function groupAttachActions($groupIds, $actionIds, $accountId = null)
    {

        $query = Group::with(['actions' => function ($q) use ($actionIds) {
            return $q->whereIn('id', $actionIds);
        }])->whereIn('id', $groupIds);
        if (null != $accountId) {
            $query->where('account_id', $accountId);
        }
        $groups = $query->get();

        foreach ($groups as $group) {
            foreach ($actionIds as $actionId) {
                $action = $group->actions->first(function ($value) use ($actionId) {
                    return $value->id == $actionId;
                });
                if (null == $action) {
                    $group->actions()->attach($actionId);
                }
            }
            $group->load(['actions' => function ($q) use ($actionIds) {
                return $q->whereIn('id', $actionIds);
            }]);
        }
        return $groups;
    }

    /**
     * @param $groupIds
     * @param $actionIds
     * @param null $accountId
     * @return Group[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function groupDetachActions($groupIds, $actionIds, $accountId = null)
    {
        $query = Group::with(['users' => function ($q) use ($actionIds) {
            return $q->whereIn('id', $actionIds);
        }])->whereIn('id', $groupIds);
        if (null != $accountId) {
            $query->where('account_id', $accountId);
        }
        $groups = $query->get();

        foreach ($groups as $group) {
            foreach ($actionIds as $actionId) {
                $action = $group->actions->first(function ($value) use ($actionId) {
                    return $value->id == $actionId;
                });
                if (null != $action) {
                    $group->actions()->detach($actionId);
                }
            }
            $group->load('actions');
        }
        return $groups;
    }

}