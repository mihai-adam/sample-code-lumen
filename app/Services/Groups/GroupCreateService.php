<?php

namespace App\Services\Groups;

use App\Group;
use App\Services\Service;

/**
 * Class GroupCreateService
 * @package App\Services\Groups
 */
class GroupCreateService extends Service
{
    /**
     * @var
     */
    protected $accountId;

    /**
     * @var
     */
    protected $groupName;

    /**
     * @var
     */
    protected $groupDescription;

    /**
     * @param $accountId
     * @return $this
     */
    public function setAccountId($accountId)
    {
        $this->accountId = $accountId;
        return $this;
    }

    /**
     * @param $data
     * @return $this
     */
    public function setGroupName($data)
    {
        $this->groupName = $data;
        return $this;
    }

    /**
     * @param $data
     * @return $this
     */
    public function setGroupDescription($data)
    {
        $this->groupDescription = $data;
        return $this;
    }

    /**
     * @return Group
     */
    public function run()
    {

        $model = new Group();
        $model->account_id = $this->accountId;
        $model->name = $this->groupName;
        $model->description = $this->groupDescription;
        $model->save();
        return $model;

    }

}