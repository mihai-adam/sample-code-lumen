<?php

namespace App\Services\Groups;

use App\Group;
use App\Services\Service;

/**
 * Class GroupUpdateService
 * @package App\Services\Groups
 */
class GroupUpdateService extends Service
{
    /**
     * @var Group
     */
    protected $group;

    /**
     * @var
     */
    protected $accountId;

    /**
     * @var
     */
    protected $groupName;

    /**
     * @var
     */
    protected $groupDescription;

    /**
     * GroupUpdateService constructor.
     * @param Group $group
     */
    public function __construct(Group $group)
    {
        $this->group = $group;
    }

    /**
     * @param $accountId
     * @return $this
     */
    public function setAccountId($accountId)
    {
        $this->accountId = $accountId;
        return $this;
    }

    /**
     * @param $data
     * @return $this
     */
    public function setGroupName($data)
    {
        $this->groupName = $data;
        return $this;
    }

    /**
     * @param $data
     * @return $this
     */
    public function setGroupDescription($data)
    {
        $this->groupDescription = $data;
        return $this;
    }

    /**
     * @return Group
     */
    public function run()
    {
        if (null != $this->accountId) {
            $this->group->account_id = $this->accountId;
        }
        if (null != $this->groupName) {

            if ($this->groupName != $this->group->name) {
                $this->group->name = $this->groupName;
            }
        }
        if (null != $this->groupDescription) {
            $this->group->description = $this->groupDescription;
        }
        $this->group->save();
        return $this->group;
    }

}