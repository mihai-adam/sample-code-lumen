<?php

namespace App\Services\Accounts;

use App\Account;
use App\Services\Models\ServiceCreate;

/**
 * Class AccountCreateService
 * @package App\Services\Accounts
 */
class AccountCreateService extends ServiceCreate
{

    /**
     * @return Account|\App\Model
     */
    protected function model()
    {
        return new Account();
    }

    /**
     * @param $data
     * @return array|mixed
     */
    protected function row($data)
    {
        return [
            'id' => $this->id(),
            'batch_id' => $this->batchId(),
            'name' => $data['name'],
            'picture' => isset($data['picture']) ? $data['picture'] : null,
            'created_at' => $this->now()->toDateTimeString(),
            'updated_at' => $this->now()->toDateTimeString()
        ];
    }
}