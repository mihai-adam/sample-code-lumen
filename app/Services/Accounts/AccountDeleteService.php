<?php

namespace App\Services\Accounts;

use App\Account;
use App\Services\Service;
use Carbon\Carbon;

/**
 * Class AccountDeleteService
 * @package App\Services\Accounts
 */
class AccountDeleteService
{

    protected $count = 0;

    /**
     * @param array $id
     * @return AccountDeleteService
     */
    public function run($id = [])
    {
        $now = Carbon::now('utc');

        $updates = [
            'updated_at' => $now->toDateTimeString(),
            'deleted_at' => $now->toDateTimeString()
        ];
        $this->count = Account::whereIn('id', $id)
            ->update($updates);

        return $this;
    }


    public function count()
    {
        return $this->count;
    }
}