<?php

namespace App\Services\Accounts;


use App\Account;
use App\Services\Exceptions\ServiceException;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

class AccountUpdateService
{

    protected $fields = [
        'name',
        'picture',
        'suspend'
    ];

    protected $input = [];
    protected $id = [];

    protected $distinct = [];

    protected $now;

    /**
     * @var Collection
     */
    protected $accounts;

    public function __construct($input = [])
    {
        foreach ($input as $row) {
            $this->id[] = $row['id'];
            foreach ($this->fields as $field) {
                if (isset($row[$field])) {
                    if (!isset($this->distinct[$field])) {
                        $this->distinct[$field] = [];
                    }
                    if (!in_array($row[$field], $this->distinct[$field])) {
                        $this->distinct[$field][] = $row[$field];
                    }

                }
            }
        }
        if (count($input) != count($this->id)) {
            $this->handleDuplicateId($input);
        }
        $this->input = $input;
    }

    /**
     * @return null|Collection
     */
    protected function accounts()
    {
        if (null == $this->accounts) {
            $id = [];
            foreach ($this->input as $row) {
                if (isset($row['id'])) {
                    $id[] = $row['id'];
                }
            }
            $this->accounts = Account::whereIn('id', $id)
                ->get();
        }
        return $this->accounts;
    }

    /**
     * This will handle the case when id is duplicated
     * @todo implement this functionality
     * @todo for now the UpdateRequestValidator prevent this scenario
     */
    protected function handleDuplicateId()
    {

    }

    /**
     * @return Account|\App\Model
     */
    protected function model()
    {
        return new Account();
    }

    /**
     * Update accounts
     * @return $this
     */
    public function run()
    {
        $bulk = true;
        foreach ($this->distinct as $value) {
            if (count($value) > 1) {
                $bulk = false;
                break;
            }
        }
        if ($bulk) {
            $this->updateBulk();
        } else $this->updateEach();
        return $this;
    }


    /**
     * This should use a standard single query to perform update
     * for all accounts
     * @todo optimise this query
     * @return $this
     */
    protected function updateBulk()
    {
        //get distinct value
        $rows = [];
        foreach ($this->distinct as $field => $value) {
            if (1 == count($value)) {
                $rows[$field] = $value[0];
            }
        }
        $now = Carbon::now('utc');
        $row['updated_at'] = $now->toDateTimeString();

        $this->model()
            ->whereIn('id', $this->id)
            ->update($rows);

        //existing account are not longer needed since there ware updated
        //make them null to force reload when send the response
        $this->accounts = null;

        return $this;
    }

    /**
     * This will run method update for each account
     * Should be the second option if updateBulk is not available
     * @return $this
     */
    protected function updateEach()
    {
        $this->accounts()->each(function ($account) {
            $row = array_first($this->input, function ($item) use ($account) {
                return $account->id == $item['id'];
            });
            unset($row['id']);
            $account->update($row);
        });
        return $this;
    }


    /**
     * If account collection already calculated in other palce,
     * stori it to prevent load from db
     * @param Collection $accounts
     * @return AccountUpdateService
     */
    public function setAccounts(Collection $accounts)
    {
        $this->accounts = $accounts;
        return $this;
    }

    /**
     * Attempt to pick one record
     * @return mixed
     * @throws ServiceException
     */
    public function getOne()
    {
        $updated = $this->accounts();
        if (1 == $updated->count()) {
            return $updated[0];
        } else {
            $message = 'Unable to get one account; ' . $updated->count() . ' accounts ware created';
            throw new ServiceException($message);
        }
    }

    /**
     * @return Collection|null
     */
    public function getUpdated()
    {
        return $this->accounts();
    }

}