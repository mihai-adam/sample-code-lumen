<?php

namespace App\Services;

use App\Account;

use App\Http\Filters\Manager;
use App\Http\Filters\SqlFilter;
use App\Services\Accounts\AccountIndexService;
use App\Services\Accounts\AccountCreateService;
use App\Services\Accounts\AccountUpdateService;
use App\Services\Accounts\AccountDeleteService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class AccountService
 * @package App\Services
 */
class AccountService extends ServiceAbstract
{

    /**
     * @param Manager $manager
     * @return mixed
     * @throws \App\Http\Filters\Exceptions\HttpFilterException
     */
    public function accountIndex(Manager $manager)
    {
        $query = Account::select();
        $filters = $manager->collect([
            'sort',
            'limit',
            'offset',
            'name',
            'picture',
            'suspended_at'
        ]);

        $filters->each(function ($filter) use ($query) {
            SqlFilter::applyToBuilder($query, $filter);
        });
        return $query;
    }

    /**
     * @param $data
     * @return AccountCreateService
     */
    public function accountCreate($data)
    {
        $service = new AccountCreateService();
        return $service->run($data);

    }

    /**
     * @param $accountId
     * @return mixed
     */
    public function accountShow($accountId)
    {
        return Account::findOrFail($accountId);
    }

    /**
     * @param array $params
     * @return AccountUpdateService
     */
    public function accountUpdate($params, Collection $target = null)
    {
        $service = new AccountUpdateService($params);
        if (null != $target) {
            $service->setAccounts($target);
        }
        return $service->run();
    }

    /**
     * @param array $id
     * @return bool
     */
    public function accountDelete($id = [])
    {
        $service = new AccountDeleteService();
        return $service->run($id);
    }
}