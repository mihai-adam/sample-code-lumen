<?php

namespace App\Services\Users\Access;

use App\Group;
use App\Action;

/**
 * Class UserAccess
 * @package App\Services\Users\Access
 */
class UserAccess
{

    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $slug;

    /**
     * @var string
     */
    protected $namespace;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var array
     */
    protected $groups = [];

    /**
     * @var bool
     */
    protected $action = false;

    /**
     * @var bool
     */
    protected $enabled = false;

    /**
     * UserAccess constructor.
     * @param Action $action
     */
    public function __construct(Action $action)
    {
        $this->id = $action->id;
        $this->slug = $action->slug;
        $this->namespace = $action->namespace;
        $this->name = $action->name;
        $this->description = $action->description;

    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @return string
     */
    public function getNamespace()
    {
        return $this->namespace;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return array
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * @return bool
     */
    public function isEnabledByGroup()
    {
        return $this->isEnabled() && !empty($this->groups);
    }

    /**
     * @return bool
     */
    public function isEnabledByAction()
    {
        return $this->isEnabled() && $this->action;
    }

    /**
     * @return $this
     */
    public function enable()
    {
        if ($this->isDisabled()) {
            $this->enabled = true;
        }
        return $this;
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    public function isDisabled()
    {
        return !$this->enabled;
    }

    /**
     * @param Group $group
     * @return UserAccess
     */
    public function enableByGroup(Group $group)
    {
        $this->groups[] = $group;
        return $this->enable();
    }

    /**
     * @return UserAccess
     */
    public function enableByAction()
    {
        $this->action = true;
        return $this->enable();
    }

}