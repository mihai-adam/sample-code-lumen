<?php

namespace App\Services\Users;

use App\Services\Models\ServiceCreate;
use App\User;

/**
 * Class UserCreateService
 * @package App\Services\Users
 */
class UserCreateService extends ServiceCreate
{

    protected $confirmed;

    public function __construct($confirmed = false)
    {
        $this->confirmed = boolval($confirmed);

    }

    protected function model()
    {
        return new User();
    }

    /**
     * @param $data
     * @return array
     */
    public function row($data)
    {
        return [
            'id' => $this->id(),
            'batch_id' => $this->batchId(),
            'account_id' => $data['account_id'],
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => $data['password'],
            'phone' => isset($data['phone']) ? $data['phone'] : null,
            'picture' => isset($data['picture']) ? $data['picture'] : null,
            'created_at' => $this->now()->toDateTimeString(),
            'confirmed_at' => $this->confirmed ? $this->now()->toDateTimeString() : null,
            'updated_at' => $this->now()->toDateTimeString()
        ];
    }


}