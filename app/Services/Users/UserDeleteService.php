<?php

namespace App\Services\Users;

use App\Services\Service;
use Illuminate\Support\Collection;

/**
 * Class UserDeleteService
 * @package App\Services
 *
 * @property Collection $collection
 */
class UserDeleteService extends Service
{

    /**
     * @var
     */
    protected $collection;

    /**
     * @param Collection $collection
     * @return $this
     */
    public function setCollection(Collection $collection)
    {
        $this->collection = $collection;
        return $this;
    }

    /**
     * @return bool
     */
    public function run()
    {
        if (!$this->collection->isEmpty()) {
            $this->collection->each(function ($g) {
                $g->delete();
            });
        }
        return true;
    }
}