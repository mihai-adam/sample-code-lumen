<?php

namespace App\Services\Users;

use App\User;
use App\Services\Service;

/**
 * Class AccountCreateService
 * @package App\Services
 *
 */
class UserUpdateService extends Service
{

    /**
     * @var User
     */
    protected $user;

    /**
     * UserUpdateService constructor.
     * @param User $user
     * @param array $attributes
     */
    public function __construct(User $user, $attributes = [])
    {
        $this->user = $user;
        $this->setAttributes($attributes);
    }

    /**
     * @return User
     */
    public function run()
    {
        foreach ($this->attributes as $name => $value) {
            $this->user->{$name} = $value;
        }
        try {
            $this->user->save();
        } catch (\Exception $exception) {
            dump($exception->getMessage());
        }
        return $this->user;
    }


}