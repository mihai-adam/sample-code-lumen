<?php

namespace App\Services\Users;

use App\User;
use App\Action;
use Illuminate\Support\Collection;
use App\Services\Users\Access\UserAccess;

/**
 * Class UserAccessService
 * @package App\Services\Users
 */
class UserAccessService
{

    /**
     * @var User
     */
    private $user;

    /**
     * @var array
     */
    private $access = [];

    /**
     * UserAccessService constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return Collection
     */
    public function getAccess()
    {
        $output = new Collection();
        foreach ($this->access as $access) {
            $output->push($access);
        }
        return $output;
    }

    /**
     * @return Collection
     */
    public function run()
    {
        return $this->bootAvailableActions()
            ->pushGroupsAccess()
            ->pushUserAccess()
            ->getAccess();
    }

    /**
     * @param User $user
     * @return UserAccessService
     */
    public static function factory(User $user)
    {
        return new self($user);
    }

    /**
     * @return $this
     */
    protected function bootAvailableActions()
    {
        $available = Action::all();
        foreach ($available as $action) {
            $this->access[$action->id] = new UserAccess($action);
        }
        return $this;
    }

    /**
     * @return $this
     */
    protected function pushGroupsAccess()
    {
        foreach ($this->user->groups as $group) {
            foreach ($group->actions as $action) {
                $this->access[$action->id]->enableByGroup($group);
            }
        }
        return $this;
    }

    /**
     * @return $this
     */
    protected function pushUserAccess()
    {
        foreach ($this->user->actions as $action) {
            $this->access[$action->id]->enableByAction();
        }
        return $this;
    }

}