<?php

namespace App\Services;

use App\Http\Requests\Request;

/**
 * Class Service
 * @package App\Services
 */
abstract class ServiceAbstract
{

    /**
     * @var Request
     */
    protected $request;

    /**
     * @param Request $request
     * @return $this
     */
    public function setRequest(Request $request)
    {
        $this->request = $request;
        return $this;
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        if (null == $this->request) {
            $request = app('request');
            $this->setRequest($request);
        }
        return $this->request;
    }

}