<?php

namespace App;

/**
 * App\Action
 *
 * Class Action
 *
 * @package App
 *
 * @property string $id
 * @property string $slug
 * @property string $namespace
 * @property string $name
 * @property string $description
 */
class Action extends Model
{
    /**
     * @var bool
     */
    protected $hasSysId = true;

    /**
     * @var bool
     */
    protected $hasBatchId = true;

    /**
     * @var string
     */
    protected $table = 'actions';

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var bool
     */
    public $timestamps = false;

    public $fillable = [
        'slug',
        'name',
        'namespace',
        'description'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function groups()
    {
        return $this->belongsToMany(Group::class, 'group_actions');
    }
}