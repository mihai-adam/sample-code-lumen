<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/
$factory->define(App\Account::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->unique()->company,
    ];
});


$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [

        'name' => $faker->firstName . ' ' . $faker->lastName,
        'email' => $faker->unique()->email,
        'password' => 'password'

    ];
});

$factory->define(App\Group::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->text(20),
        'description' => $faker->realText(200, 1)
    ];
});
