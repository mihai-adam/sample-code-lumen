<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     *
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->string('id', 24)->unique()->index()->primary();
            $table->string('batch_id', 16);
            $table->string('account_id', 24)->index();

            $table->string('name', 64);
            $table->string('email', 64)->unique()->index();
            $table->string('password');

            $table->timestamps();
            $table->timestamp('suspended_at')->nullable();
            $table->softDeletes();

            $table->foreign('account_id')->references('id')->on('accounts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!app()->environment('testing')) {
            Schema::table('users', function (Blueprint $table) {
                $table->dropForeign(['account_id']);
            });
        }
        Schema::drop('users');
    }
}
