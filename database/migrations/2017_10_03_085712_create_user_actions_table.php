<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserActionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_actions', function (Blueprint $table) {
            $table->string('user_id', 24)->index();
            $table->string('action_id', 24)->index();


            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('action_id')->references('id')->on('actions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!app()->environment('testing')) {
            Schema::table('user_actions', function (Blueprint $table) {
                try {
                    $table->dropForeign(['user_id']);
                    $table->dropForeign(['action_id']);
                } catch (\Exception $e) {

                }
            });
        }
        Schema::drop('user_actions');
    }
}
