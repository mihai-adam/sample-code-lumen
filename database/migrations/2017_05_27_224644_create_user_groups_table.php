<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_groups', function (Blueprint $table) {
            $table->string('user_id', 24)->index();
            $table->string('group_id', 24)->index();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('group_id')->references('id')->on('groups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!app()->environment('testing')) {
            Schema::table('user_groups', function (Blueprint $table) {
                try {
                    $table->dropForeign(['user_id']);
                    $table->dropForeign(['group_id']);
                } catch (\Exception $e) {

                }
            });
        }
        Schema::drop('user_groups');
    }
}
