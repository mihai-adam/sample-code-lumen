<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groups', function (Blueprint $table) {
            $table->string('id', 24)->primary()->index()->unique();
            $table->string('batch_id', 16);
            $table->string('account_id', 24)->index();
            $table->string('name', 32);
            $table->text('description')->nullable();
            $table->timestamps();

            $table->foreign('account_id')->references('id')->on('accounts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!app()->environment('testing')) {
            Schema::table('groups', function (Blueprint $table) {
                $table->dropForeign(['account_id']);
            });
        }
        Schema::drop('groups');
    }
}
