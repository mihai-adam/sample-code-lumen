<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupActionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_actions', function (Blueprint $table) {
            $table->string('group_id', 24);
            $table->string('action_id', 24);


            $table->foreign('group_id')->references('id')->on('groups');
            $table->foreign('action_id')->references('id')->on('actions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!app()->environment('testing')) {
            Schema::table('group_actions', function (Blueprint $table) {
                try {
                    $table->dropForeign(['action_id']);
                    $table->dropForeign(['group_id']);
                } catch (\Exception $e) {

                }
            });
        }
        Schema::drop('group_actions');
    }
}
