<?php

use Illuminate\Database\Seeder;
use App\Action;

class ActionsTableSeeder extends Seeder
{
    private $actions = [
        'account-edit' => 'Allow user to edit account',
        'account-suspend' => 'Allow user to suspend the entire account',
        'account-pay' => 'Allow user to pay for account',

        'account-user-create' => 'Allow user to create other users',
        'account-user-view-all' => 'Allow user to view all other users',
        'account-user-edit-all' => 'Allow user to edit other users',
        'account-user-delete-all' => 'Allow user or delete other users',
        'account-user-delete-own' => 'Allow user to delete himself from the system',
        'account-user-suspend-all' => 'Allow user to suspend other users from the system',
        'account-user-suspend-own' => 'Allow user to suspend himself from the system',

        'account-group-create' => 'Allow user to create access groups',
        'account-group-view-all' => 'Allow user to view all access groups',
        'account-group-view-own' => 'Allow user to view access groups that he belongs to',
        'account-group-edit-all' => 'Allow user to edit all access groups',
        'account-group-edit-own' => 'Allow user to edit access groups that he belongs to',
        'account-group-delete' => 'Allow user to delete access groups',
    ];

    public function run()
    {
        foreach ($this->getActions() as $action) {
            $account = Action::create($action);
            $account->id = $action['id'];
            $account->save();
        }
    }

    private function getActions()
    {
        $output = [];
        $counter = 0;
        foreach ($this->actions as $item => $description) {
            $parts = explode('-', $item, 2);
            $output[] = [
                'id' => $this->generateId($counter),
                'slug' => $item,
                'namespace' => $parts[0],
                'name' => $parts[1],
                'description' => $description
            ];
            $counter++;

        }
        return $output;
    }

    private function generateId($counter)
    {
        $prefix = 'acco_01_action000000';
        $id = '';
        $length = 4 - strlen($counter);
        for ($i = 0; $i < $length; $i++) {
            $id .= '0';
        }
        return $prefix . $id . $counter;
    }
}
