<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call('ContextSeeder');
        $this->call('ActionsTableSeeder');
        $this->factory();
    }

    private function factory()
    {
        $actions = \App\Action::all();

        factory(App\Account::class, 20)->create()->each(function ($a) use ($actions) {
            factory(App\User::class, rand(2, 20))->create([
                'account_id' => $a->id
            ])->each(function ($user) use ($actions) {
                $elements = rand(0, $actions->count());
                $user->actions()->attach($actions->random($elements));
            });

            factory(App\Group::class, rand(1, 5))->create([
                'account_id' => $a->id
            ])->each(function ($group) use ($actions) {
                $elements = rand(0, $actions->count());
                $group->actions()->attach($actions->random($elements));
            });
        });

        $users = \App\User::with(['account' => function ($q) {
            return $q->with('groups');
        }])->get();

        $users->each(function ($user){
            $groups = $user->account->groups;
            $elements = rand(0, $groups->count());
            $user->groups()->attach($groups->random($elements));
        });
    }

}
