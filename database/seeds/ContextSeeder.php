<?php

use Illuminate\Database\Seeder;

class ContextSeeder extends Seeder
{

    public function run()
    {

        $account = App\Account::create([
            'name' => 'System Account',

        ]);
        $account->id = 'acco_01_account000000000';
        $account->save();

        $admin = \App\User::create([
            'account_id' => $account->id,
            'name' => 'Adam Mihai',
            'email' => 'mihai.adam@dynamicart.ro',
            'password' => 'password'
        ]);
        $admin->id = 'acco_01_user000000000000';
        $admin->save();

        $site = \App\User::create([
            'account_id' => $account->id,
            'name' => 'site user',
            'email' => 'site.admin@dynamicart.ro',
            'password' => 'password',
        ]);
        $site->id = 'acco_01_user000000000001';
        $site->save();

        $group = \App\Group::create([
            'name' => 'Administrators',
            'account_id' => $account->id,
            'description' => 'System account administrators'
        ]);
        $group->id = 'acco_01_group00000000000';
        $group->save();

    }
}